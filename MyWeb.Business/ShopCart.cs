﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace MyWeb.Business
{
    public class ShopCart
    {
        //Create shop cart
        public static DataTable CreateCart()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("Name");
            dt.Columns.Add("Image");
            dt.Columns.Add("Number");
            dt.Columns.Add("Price");
            dt.Columns.Add("Money");
            return dt;
        }

        //Add product to cart
        public static void AddToCart(ref DataTable dtCart, string ProId, string proName, string proImg, double proPrice)
        {

            if (dtCart.Rows.Count > 0)
                for (int i = 0; i < dtCart.Rows.Count; i++)
                {
                    if (dtCart.Rows[i]["ID"].ToString() == ProId)
                    {
                        dtCart.Rows[i]["Number"] = int.Parse(dtCart.Rows[i]["Number"].ToString()) + 1;
                        dtCart.Rows[i]["Money"] = double.Parse((int.Parse(dtCart.Rows[i]["Number"].ToString()) * int.Parse(dtCart.Rows[i]["Price"].ToString())).ToString());
                        return;
                    }
                }
            DataRow dr = dtCart.NewRow();
            dr["ID"] = ProId;
            dr["Name"] = proName;
            dr["Image"] = proImg;
            dr["Number"] = 1;
            dr["Price"] = proPrice;
            dr["Money"] = proPrice;
            dtCart.Rows.Add(dr);
            return;
        }

        //Remove product 
        public static void DeleteProduct(ref DataTable dtCart, int ProId)
        {
            for (int i = 0; i < dtCart.Rows.Count; i++)
            {
                if (dtCart.Rows[i]["ID"].ToString() == ProId.ToString())
                {
                    dtCart.Rows.RemoveAt(i);
                    return;
                }
            }
        }

        //Update product
        public static void UpdateProduct(ref DataTable dtCart, int ProId, int number)
        {
            for (int i = 0; i < dtCart.Rows.Count; i++)
            {
                if (dtCart.Rows[i]["ID"].ToString() == ProId.ToString())
                {
                    double oldMoney = double.Parse(dtCart.Rows[i]["Money"].ToString());
                    
                    int oldNumber = int.Parse(dtCart.Rows[i]["Number"].ToString());

                    dtCart.Rows[i]["Money"] =oldMoney  + ((number - oldNumber) * oldMoney);
                    dtCart.Rows[i]["Number"] = number.ToString();
                    return;
                }
            }
        }
        public static double TotalMoney(DataTable dtCart) {
            double re = 0;
            foreach (DataRow row in dtCart.Rows)
            {
                re += double.Parse(row["Money"].ToString());
            }
            return re;
        }
    }
}