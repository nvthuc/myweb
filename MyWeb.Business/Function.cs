﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Data.Entity;

using MyWeb.Data;
namespace MyWeb.Business
{
    public class Function
    {
       
        public static string NameToTag(string strName)
        {
            string strReturn = "";
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            strReturn = Regex.Replace(strName, "[^\\w\\s]", string.Empty).Replace(" ", "-").ToLower();
            string strFormD = strReturn.Normalize(System.Text.NormalizationForm.FormD);
            return regex.Replace(strFormD, string.Empty).Replace("đ", "d");
        }
        public static void WebAlert(HttpResponse Response, string mess)
        {
            Response.Write("<script type='text/javascript'>alert('" + mess + "');</script>");
            return;
        }
        public static void WebAlert(string mess)
        {
            HttpContext.Current.Response.Write("<script type='text/javascript'>alert('" + mess + "');</script>");

        }
        public static string GenTreeCode(string PanrentTreeCode, string TreeCode)
        {
            while (TreeCode.Length < 5)
            {
                TreeCode = "0" + TreeCode;
            }
            return PanrentTreeCode + TreeCode;
        }
        public static int TreeCodeToOrd(string TreeCode)
        {
            return int.Parse(TreeCode.Substring(TreeCode.Length - 5, 5));
        }
        public static string OrdToTreeCode(string PanrentTreeCode, int ord)
        {
            string TreeCode = ord.ToString();
            while (TreeCode.Length < 5)
            {
                TreeCode = "0" + TreeCode;
            }
            return PanrentTreeCode + TreeCode;
        }
        
    }
}