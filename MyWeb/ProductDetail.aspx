﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/main.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="MyWeb.ProductDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <link href="Scripts/cz/multizoom.css" rel="stylesheet" />
    <link href="Scripts/cz/thumbelina.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-my-panel">
                <div class="panel-heading">
                    <h2 class="panel-title" style="font-size: 22px;">
                        <asp:Literal Text="" runat="server" ID="ltrCateName" /></h2>
                </div>
                <div class="panel-body">
                    <div class="row col-md-12">
                        <h3 style="font-size: 20px; color: #768126; margin-top: 0;">
                            <asp:Literal Text="" runat="server" ID="ltrProName" /></h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="targetarea page-menu-item" style="border: 1px solid #eee; padding:3px;">
                                    <asp:Literal Text="" runat="server" ID="ltrImgTarget" />
                                   
                                </div>
                                <br />
                                <br />
                                <div class="multizoom1 thumbs">
                                    <asp:Literal Text="Text" runat="server" ID="ltrImgPro" />
                                    <asp:Repeater runat="server" ID="rptProImg">
                                        <ItemTemplate>
                                          <a href='<%#Eval("Imageoriginal") %>' >
                                                <img src='<%#Eval("Imagethumb") %>' width='50px' height='50px' alt="" /></a>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                   
                                </div>
                            </div>
                            <div class="col-md-5 col-md-offset-1">
                                <asp:Repeater runat="server" ID="rptProProps">
                                    <ItemTemplate>
                                        <div class="col-md-6"><%#Eval("Lable")%> </div>
                                        <div class="col-md-6"><%#Eval("Content") %></div>
                                        <div class="clearfix"></div>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <br />
                                <div class="col-md-12">
                                    <asp:Literal Text="" runat="server" ID="ltrDatMua" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row col-md-12">
                        <asp:Literal Text="" runat="server" ID="ltrProDetail" />
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12">
            <div class="panel panel-my-panel">
                <div class="panel-heading">
                    <h2 class="panel-title">Sản phẩm cùng loại</h2>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <asp:Repeater runat="server" ID="rptProduct">
                    <HeaderTemplate></HeaderTemplate>
                    <ItemTemplate>
                        <div class="col-md-3 pro-item">
                            <a href='/san-pham-ct/<%#Eval("Keyname") %>-piden<%#Eval("Proid") %>.html' title='<%#Eval("Proname") %>'>
                                <div class="thumbnail menu-item-thumb">
                                    <img src="<%#Eval("Proimage") %>" />
                                </div>
                            </a>
                            <h5><a href='/san-pham-ct/<%#Eval("Keyname") %>-ident<%#Eval("Proid") %>.html' title='<%#Eval("Proname") %>'><%#Eval("Proname") %> </a>
                            </h5>
                            <p>Giá bán: <%#Eval("Price") %>d</p>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate></FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <script src="/Scripts/cz/multizoom.js"></script>
    <script src="/Scripts/cz/thumbelina.js"></script>
    <script type="text/javascript">

        jQuery(document).ready(function ($) {


            $('#multizoom1').addimagezoom({ // multi-zoom: options same as for previous Featured Image Zoomer's addimagezoom unless noted as '- new'
                //descArea: '#description', // description selector (optional - but required if descriptions are used) - new
                speed: 1500, // duration of fade in for new zoomable images (in milliseconds, optional) - new
                descpos: true, // if set to true - description position follows image position at a set distance, defaults to false (optional) - new
                imagevertcenter: true, // zoomable image centers vertically in its container (optional) - new
                magvertcenter: true, // magnified area centers vertically in relation to the zoomable image (optional) - new
                zoomrange: [3, 10],
                magnifiersize: [250, 250],
                magnifierpos: 'right',
                cursorshadecolor: '#fdffd5',
                cursorshade: true //<-- No comma after last option!
            });



        })

    </script>
</asp:Content>
