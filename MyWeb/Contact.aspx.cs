﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MyWeb.Data;
namespace MyWeb
{
    public partial class Contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (var ctx = new MyWebEntities())
            {
                var i = new Data.Idea();
                i.Fullname = Name.Text;
                i.Phone = Phone.Text;
                i.Idea1 = Idea.Text;
                i.Status = 0;
                ctx.Ideas.AddObject(i);
                ctx.SaveChanges();
                pnForm.Visible = false;
                pnAlert.Visible = true;
            }

        }
    }
}