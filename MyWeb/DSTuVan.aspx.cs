﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MyWeb.Data;
namespace MyWeb
{
    public partial class DSTuVan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var ctx = new MyWebEntities())
            {
                rptTv.DataSource = ctx.News.ToList();
                rptTv.DataBind();
            }

        }
    }
}