﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MyWeb.Data;
namespace MyWeb
{
    public partial class TuVan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var r = RouteData.Values["id"];
            if (r == null)
            {
                return;
            }
            int tvId = int.Parse(r.ToString());
            using (var ctx = new MyWebEntities())
            {
                var tv = ctx.News.First(m => m.Nid == tvId);
                ltrTItle.Text = tv.Title;
                ltrText.Text = tv.Detail;
            }

        }
    }
}