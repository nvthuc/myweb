﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.Data;
using System.Web.Services;
using System.Data;
namespace MyWeb
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int pageCount = 0;
                int tmpCount = 0;
                int pageIndex = 1;
                var rqPage = Request.QueryString["page"];
                if (rqPage != null)
                {
                    pageIndex = int.Parse(rqPage.ToString());
                }
                var Section = RouteData.Values["Section"];
                if (Section == null) //trang chu
                {
                    //Hide paging
                    ltrPagination.Text = "";
                    //Load 4 Cate
                    int pagePanelCount = 0;
                    rptParent.DataSource = CreatePanelDS(GetCateByCate(null, 1, 4, 5, ref pagePanelCount));
                    rptParent.DataBind();
                    int pageProCount = 0;
                    LoadProductsByCate(1, 4, ref pageProCount);
                }
                else
                {
                    switch (Section.ToString())
                    {
                        case "Cates":
                            {
                                pageCount = 0;
                                //Cate panel
                                rptParent.DataSource = CreatePanelDS(GetCateByCate(null, pageIndex, 4, 5, ref pageCount));
                                rptParent.DataBind();
                                //Product List
                                LoadProductsByCate(1, 4, ref tmpCount);
                                ltrPagination.Text = LoadPagingation(pageIndex, pageCount);
                                break;
                            }
                        case "ProByCate":
                            {
                                int Cid = int.Parse(RouteData.Values["cid"].ToString());
                                var c = GetCateById(Cid);
                                pageCount = 0;
                                //Cate panel
                                rptParent.DataSource = CreatePanelDS(c.Catname, Cid);
                                rptParent.DataBind();
                                //Product List
                                LoadProductsByCate(pageIndex, 16, ref pageCount);

                                ltrPagination.Text = LoadPagingation(pageIndex, pageCount);
                                break;
                            }
                        case "ProByBrand":
                            {
                                int bid = int.Parse(RouteData.Values["bid"].ToString());
                                var b = GetBrandById(bid);
                                pageCount = 0;
                                //Cate panel
                                rptParent.DataSource = CreatePanelDS(b.Tite, bid);
                                rptParent.DataBind();
                                //Product List
                                LoadProductsByBrand(pageIndex, 16, ref pageCount);

                                ltrPagination.Text = LoadPagingation(pageIndex, pageCount);
                                break;
                            }
                        case "Pros":
                            {
                               
                                rptParent.DataSource = CreatePanelDS("Sản phẩm", null);
                                rptParent.DataBind();
                                pageCount = 0;
                                LoadProducts(pageIndex, 16, ref pageCount);
                                ltrPagination.Text = LoadPagingation(pageIndex, pageCount);
                                break;
                            }
                        case "CatesByCate":
                            {
                                pageCount = 0;
                                int Catid = int.Parse(RouteData.Values["cid"].ToString());

                                //Cate panel
                                rptParent.DataSource = CreatePanelDS(GetCateByCate(Catid, pageIndex, 4, 5, ref pageCount));
                                rptParent.DataBind();
                                //Product List
                                LoadProductsByCate(1, 4, ref tmpCount);
                                ltrPagination.Text = LoadPagingation(pageIndex, pageCount);
                                break;
                            }
                        case "Brands":
                            {
                                pageCount = 0;
                                //Brand panel
                                rptParent.DataSource = CreatePanelDS(GetBrands(pageIndex, 4, ref pageCount));
                                rptParent.DataBind();
                                //Product List
                                LoadProductsByBrand(1, 4, ref tmpCount);
                                ltrPagination.Text = LoadPagingation(pageIndex, pageCount);
                                break;
                            }
                        default:
                            break;
                    }
                }
            }
        }

        private void LoadProducts(int pageIndex, int pageSize, ref int pageCount)
        {
            foreach (RepeaterItem e in rptParent.Items)
            {
                var rptProduct = e.FindControl("rptProduct") as Repeater;
                rptProduct.DataSource = GetProducts(pageIndex, pageSize, ref pageCount);
                rptProduct.DataBind();
            }

        }

        private List<Data.Product> GetProducts(int pageIndex, int pageSize, ref int pageCount)
        {
            using (var ctx = new MyWebEntities())
            {
                int tt = ctx.Products.Where(m => m.Proactive == 1).Count();
                if (tt >0 )
                {
                    pageCount = (tt % pageSize) == 0 ? (tt / pageSize) : ((tt / pageSize) + 1);
                }
                else
                {
                    pageCount = 0;
                }
                return ctx.Products.Where(m => m.Proactive == 1).OrderBy(m => m.Proord).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }

        }

        private Data.Brand GetBrandById(int bid)
        {
            using (var ctx = new MyWebEntities())
            {
                return ctx.Brands.First(m => m.Brandid == bid);
            }

        }

        private string LoadPagingation(int PageIndex, int PageCount)
        {
            if (PageCount > 0)
            {
                //First
                string result = "<li><a href='?page=1'>&laquo;</a></li>";

                //... if > 1
                if (PageIndex > 3)
                {
                    result += "<li><a>...</a></li>";
                }
                if (PageIndex == 3)
                {
                    result += "<li><a href='?page=1'>1</a></li>";
                }
                //1 not view
                if (PageIndex > 1)
                {
                    result += "<li><a href='?page=" + (PageIndex - 1) + "'>" + (PageIndex - 1) + "</a></li>";
                }
                //if (PageIndex == 1)
                //{
                //    ltrPagination.Text += "<li><a href='?page=1'>1</a></li>";
                //}
                //Current
                result += "<li class='active'><a href='#'>" + (PageIndex) + "</a></li>";
                //
                if (PageIndex + 1 <= PageCount)
                {
                    result += "<li><a href='?page=" + (PageIndex + 1) + "'>" + (PageIndex + 1) + "</a></li>";
                }
                if (PageIndex == 1 && PageCount >= 3)
                {
                    result += "<li><a href='?page=3'>3</a></li>";
                }
                if (PageCount > 3 && PageIndex < PageCount - 3)
                {
                    result += "<li><a>...</a></li>";
                }
                result += "<li><a href='?page=" + PageCount + "'>&raquo;</a></li>";
                return result;
            }
            else
                return "";
        }
        protected void LoadProductsByCate(int pageIndex, int pageSize, ref int pageCount)
        {
            foreach (RepeaterItem e in rptParent.Items)
            {
                var lblId = e.FindControl("lblId") as Label;
                int Id = int.Parse(lblId.Text);
                var rptProduct = e.FindControl("rptProduct") as Repeater;
                rptProduct.DataSource = GetProductByCate(Id, pageIndex, pageSize, ref pageCount);
                rptProduct.DataBind();
            }
        }
        protected void LoadProductsByBrand(int pageIndex, int pageSize, ref int pageCount)
        {
            foreach (RepeaterItem e in rptParent.Items)
            {
                var lblId = e.FindControl("lblId") as Label;
                int Id = int.Parse(lblId.Text);
                var rptProduct = e.FindControl("rptProduct") as Repeater;
                rptProduct.DataSource = GetProductByBrand(Id, pageIndex, pageSize, ref pageCount);
                rptProduct.DataBind();
            }
        }
        private List<Data.Product> GetProductByBrand(int bid, int pageIndex, int pageSize, ref int pageCount)
        {
            using (var ctx = new MyWebEntities())
            {
                int tt = ctx.Products.Where(m => m.Brandid == bid && m.Proactive == 1).Count();
                if (tt > 0)
                {
                    pageCount = tt % pageSize == 0 ? tt / pageSize : (tt / pageSize) + 1;
                }

                return ctx.Products.Where(m => m.Brandid == bid && m.Proactive == 1).OrderBy(m => m.Proord).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }
        }
        private int CountProductByBrand(int bid)
        {
            using (var ctx = new MyWebEntities())
            {
                return ctx.Products.Where(m => m.Brandid == bid).Count();
            }
        }
        private List<Data.Product> GetProductByCate(int cid, int pageIndex, int pageSize, ref int pageCount)
        {
            using (var ctx = new MyWebEntities())
            {
                var lst = new List<Data.Product>();
                foreach (var item in ctx.Products.Where(m => m.Catid == cid && m.Proactive == 1).ToList())
                {
                    lst.Add(item);
                    AddProByCate(ref lst, cid, 5);
                }
                return lst;
            }

        }

        private void AddProByCate(ref List<Product> lst, int cid, int st)
        {
            using (var ctx = new MyWebEntities())
            {
                string treecode = ctx.Categories.First(m => m.Cid == cid).Treecode;
                int lenght = treecode.Length + st;
                foreach (var item in ctx.Categories.Where(m => m.Treecode.Length == lenght && m.Treecode.StartsWith(treecode)).OrderBy(m => m.Treecode).ToList())
                {
                    int catid = item.Cid;
                    foreach (var pro in ctx.Products.Where(m => m.Catid == catid && m.Proactive == 1))
                    {
                        lst.Add(pro);
                    }
                    AddProByCate(ref lst, catid, 5);
                }
            }
        }
        private List<Data.Category> GetCateByCate(int? cid, int pageIndex, int pageSize, int st, ref int pageCount)
        {
            using (var ctx = new MyWebEntities())
            {
                var lst = new List<Data.Category>();
                if (cid != null)
                {
                    string treecode = ctx.Categories.First(m => m.Cid == cid).Treecode;
                    int lenght = treecode.Length + st;
                    foreach (var item in ctx.Categories.Where(m => m.Treecode.Length == lenght && m.Treecode.StartsWith(treecode)).OrderBy(m => m.Treecode).ToList())
                    {
                        lst.Add(item);
                    }
                }
                else
                {
                    int lenght = st;
                    foreach (var item in ctx.Categories.Where(m => m.Treecode.Length == lenght && m.Catactive == 1).OrderBy(m => m.Treecode).ToList())
                    {
                        lst.Add(item);
                    }
                }

                pageCount = lst.Count % pageSize == 0 ? (lst.Count / pageSize) : (lst.Count / pageSize + 1);
                return lst.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }

        }
        //Get list brand
        private List<Data.Brand> GetBrands(int pageIndex, int pageSize, ref int pageCount)
        {
            using (var ctx = new MyWebEntities())
            {
                var lst = new List<Data.Brand>();
                lst.AddRange(ctx.Brands.ToList());
                pageCount = lst.Count % pageSize == 0 ? (lst.Count / pageSize) : (lst.Count / pageSize + 1);
                return lst.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            }

        }
        //Get cate by Cid
        private Data.Category GetCateById(int cid)
        {
            using (var ctx = new MyWebEntities())
            {
                return ctx.Categories.First(m => m.Cid == cid);
            }

        }
        //Create datasource for panel
        private DataTable CreatePanelDS(List<Data.Category> lst)
        {
            var dt = new DataTable();
            dt.Columns.Add("title");
            dt.Columns.Add("id");
            foreach (var item in lst)
            {
                DataRow dr = dt.NewRow();
                dr["title"] = "<a href='/loai-san-pham/" + item.Keyname + "_c" + item.Cid + "' >" + item.Catname + "</a> " + "<a href='/san-pham/" + item.Keyname + "_c" + item.Cid + "' class='small'>xem thêm</a>";
                dr["id"] = item.Cid;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        private DataTable CreatePanelDS(List<Data.Brand> lst)
        {
            var dt = new DataTable();
            dt.Columns.Add("title");
            dt.Columns.Add("id");
            foreach (var item in lst)
            {
                DataRow dr = dt.NewRow();
                dr["title"] = item.Tite + "<a href='/san-pham/" + MyWeb.Business.Function.NameToTag(item.Tite) + "_b" + item.Brandid + "' class='small'>xem thêm</a>";
                dr["id"] = item.Brandid;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        private DataTable CreatePanelDS(string title, int? id)
        {
            var dt = new DataTable();
            dt.Columns.Add("title");
            dt.Columns.Add("id");
            DataRow dr = dt.NewRow();
            dr["title"] = title;
            dr["id"] = id == null ? "" : id.ToString();
            dt.Rows.Add(dr);
            return dt;
        }
    }
}