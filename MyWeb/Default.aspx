﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/main.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MyWeb.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <asp:Repeater runat="server" ID="rptParent">
            <ItemTemplate>
                <div class="col-md-12">
                    <div class="panel panel-my-panel">
                        <div class="panel-heading">
                            <h2 class="panel-title"><%#Eval("title") %> </h2>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <asp:Label Text='<%#Eval("id") %>' runat="server" ID="lblId" Visible="false" />
                     <asp:Repeater runat="server" ID="rptProduct">
                            <HeaderTemplate></HeaderTemplate>
                            <ItemTemplate>
                                <div class="col-md-3 pro-item">
                                    <a href='/san-pham/<%#Eval("Keyname") %>_p<%#Eval("Proid") %>.html' title='<%#Eval("Proname") %>'>
                                        <div class="thumbnail menu-item-thumb">
                                            <img src="<%#Eval("Proimage") %>" />
                                        </div>
                                    </a>
                                    <h5><a href='/san-pham/<%#Eval("Keyname") %>_p<%#Eval("Proid") %>.html' title='<%#Eval("Proname") %>'><%#Eval("Proname") %> </a>
                                    </h5>
                                    <p>Giá bán: <%#double.Parse(Eval("Price").ToString()).ToString("C0") %> </p>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate></FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
      <div class="col-md-12">
            <div class="pull-right">
                <ul class="pagination">
                    <asp:Literal Text="" runat="server" ID="ltrPagination" /> 
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
