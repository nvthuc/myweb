﻿function BrowseServerFile(idInput) {
    var f = new CKFinder();
    f.selectActionFunction = function (fileUrl) {
        document.getElementById(idInput).value = fileUrl;
    };
    f.popup();
}
function BrowseServerFileAndSetImage(IdInput, IdImage) {
    var f = new CKFinder();
    f.selectActionFunction = function (fileUrl) {
        document.getElementById(IdInput).value = fileUrl;
        document.getElementById(IdImage).src = fileUrl;
    };
    f.popup();
}