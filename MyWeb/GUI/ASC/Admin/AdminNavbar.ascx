﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminNavbar.ascx.cs"
    Inherits="MyWeb.ASC.AdminNavbar" %>
<div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
                class="icon-bar"></span><span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><big>Quản lí Milk Store</big></a>
    </div>
    <div class="navbar-collapse collapse">
        <p class="navbar-text  text-right navbar-right">
            Xin chào <b><a href="#" class="navbar-link">ThucNV</a></b> ! [<b><a href="/" target="_blank" class="navbar-link">Trang chủ</a></b>][<b><a href="/" class="navbar-link">Thoát</a></b>]
        </p>
        <%--<ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Settings</a></li>
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Help</a></li>
                </ul>--%>
    </div>
</div>
