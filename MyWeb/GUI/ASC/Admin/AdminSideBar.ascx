﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminSideBar.ascx.cs"
    Inherits="MyWeb.ASC.AdminSideBar" %>
<ul class="nav nav-sidebar">
    <li class="active"><a href="#">Overview</a></li>
    <li><a href="#">Reports</a></li>
    <li><a href="#">Analytics</a></li>
    <li><a href="#">Export</a></li>
</ul>
<ul class="nav nav-sidebar">
    <li><a href="Config.aspx"><span class="glyphicon glyphicon-cog"></span> Cài đặt</a></li>
    <li><a href="Tuvan.aspx"><span class="glyphicon glyphicon-info-sign"></span> Tư vấn</a></li>
    <li><a href="Feedback.aspx"><span class="glyphicon glyphicon-repeat"></span> Phản hồi</a></li>
    <li><a href="ads.aspx"><span class="glyphicon glyphicon-picture"></span> Hình ảnh</a></li>
    <li><a href="Contact.aspx"> Liên hệ</a></li>
</ul>
<ul class="nav nav-sidebar">
    <li><a href="Brand.aspx"><span class="glyphicon glyphicon-home"></span> Nhà SX</a></li>
    <li><a href="Cate.aspx"><span class="glyphicon glyphicon-list-alt"></span> Phân loại</a></li>
    <li><a href="Product.aspx"><span class="glyphicon glyphicon-th-large"></span> Sản phẩm</a></li>
</ul>
