﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.GUI.ASC
{
    public partial class Pagingation : System.Web.UI.UserControl
    {
        int PageIndex = 1;
        int PageCount = 1;
        protected void Page_Load(object sender, EventArgs e)
        {
            ltrPagination.Text = LoadPagingation(PageIndex, PageCount);
        }
        private string LoadPagingation(int PageIndex, int PageCount)
        {
            if (PageCount > 0)
            {
                //First
                string result = "<li><a href='?page=1'>&laquo;</a></li>";

                //... if > 1
                if (PageIndex > 3)
                {
                    result += "<li><a>...</a></li>";
                }
                if (PageIndex == 3)
                {
                    result += "<li><a href='?page=1'>1</a></li>";
                }
                //1 not view
                if (PageIndex > 1)
                {
                    result += "<li><a href='?page=" + (PageIndex - 1) + "'>" + (PageIndex - 1) + "</a></li>";
                }
                //if (PageIndex == 1)
                //{
                //    ltrPagination.Text += "<li><a href='?page=1'>1</a></li>";
                //}
                //Current
                result += "<li class='active'><a href='#'>" + (PageIndex) + "</a></li>";
                //
                if (PageIndex + 1 <= PageCount)
                {
                    result += "<li><a href='?page=" + (PageIndex + 1) + "'>" + (PageIndex + 1) + "</a></li>";
                }
                if (PageIndex == 1 && PageCount >= 3)
                {
                    result += "<li><a href='?page=3'>3</a></li>";
                }
                if (PageCount > 3 && PageIndex < PageCount - 3)
                {
                    result += "<li><a>...</a></li>";
                }
                result += "<li><a href='?page=" + PageCount + "'>&raquo;</a></li>";
                return result;
            }
            else
                return "";
        }
    }

}