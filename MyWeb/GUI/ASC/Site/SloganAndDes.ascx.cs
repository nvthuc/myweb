﻿using MyWeb.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.GUI.ASC.Site
{
    public partial class SloganAndDes : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var ctx = new MyWebEntities())
                {

                    //Slogan
                    var objConfig = ctx.Configs.FirstOrDefault();
                    if (objConfig != null)
                    {
                        //Slogan
                        ltrSlogan.Text = objConfig.Slogan;
                        //Slogan description
                        ltrSloganDes.Text = objConfig.Note1;  
                    }


                }
            }
        }
    }
}