﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.Data;
namespace MyWeb.GUI.ASC.Site
{
    public partial class PageMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var ctx = new MyWebEntities())
                {
                    //Cate
                    rptCate.DataSource = ctx.Categories.Where(m => m.Treecode.Length == 5).OrderBy(m => m.Catord).Take(6).ToList();
                    rptCate.DataBind();

                    //Brand
                    rptBrands.DataSource = ctx.Brands.OrderBy(m => m.Brandid).Take(6).ToList();
                    rptBrands.DataBind();

                    //Tu Van
                    rptTuVan.DataSource = ctx.News.OrderBy(m => m.Nid).Take(6).ToList();
                    rptTuVan.DataBind();

                    var config = ctx.Configs.FirstOrDefault();
                    if (config != null)
                    {
                        ltrHotline.Text = config.Tel + "<br />" + config.Fax + "<br />";
                        ltrSupport.Text = "<a href=\"skype:" + config.Email + "?chat\"><img src=\"/Images/webfile/online.png\" alt=\"Chat skype với hỗ trợ viên 1\" />Hỗ trợ 1</a>" + "<br/>" + "<a href=\"skype:" + config.Conemail + "?chat\"><img src=\"/Images/webfile/online.png\" alt=\"Chat skype với hỗ trợ viên 2\" />Hỗ trợ 2</a>";
                    }
                }

            }
        }
    }
}