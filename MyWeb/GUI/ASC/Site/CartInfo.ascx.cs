﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.GUI.ASC.Site
{
    public partial class CartInfo : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Cart"] == null)
            {
                ltrCartMoney.Text = double.Parse("0").ToString("C0");
                return;
            }
            var catr = Session["Cart"] as System.Data.DataTable;
            double total = 0;
            foreach (System.Data.DataRow row in catr.Rows)
            {
                total += double.Parse(row["Money"].ToString());
            }
            ltrCartMoney.Text = total.ToString("C0");
        }
    }
}