﻿using MyWeb.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.GUI.ASC.Site
{
    public partial class Footer : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                using (var ctx = new MyWebEntities())
                {

                    var config = ctx.Configs.FirstOrDefault();
                    if (config != null)
                    {
                        ltrCompany.Text = config.Title;
                        ltrAddress.Text = config.Address;
                        ltrNote2.Text = config.Note2;
                    }
                }

            }
        }
    }
}