﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SloganAndDes.ascx.cs" Inherits="MyWeb.GUI.ASC.Site.SloganAndDes" %>
<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
        <h2 class="text-center text-slogan">
            <asp:Literal Text="" ID="ltrSlogan" runat="server" /></h2>
        <p class="text-center">
            <asp:Literal Text="" runat="server" ID="ltrSloganDes" />
        </p>
    </div>
    <div class="clearfix"></div>
</div>
