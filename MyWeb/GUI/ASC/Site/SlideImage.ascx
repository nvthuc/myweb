﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SlideImage.ascx.cs" Inherits="MyWeb.GUI.ASC.Site.SlideImage" %>
<div class="row">
    <div class="col-100" id="slider1_container" style="position: relative; top: 0; left: 0; width: 1000px; height: 300px; overflow: hidden;">
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; background-color: #000000; top: 0; left: 0; width: 100%; height: 100%;">
            </div>
            <div style="position: absolute; display: block; background: url(/Images/webfile/loading.gif) no-repeat center center; top: 0; left: 0; width: 100%; height: 100%;">
            </div>
        </div>
        <div u="slides" style="cursor: move; position: absolute; left: 0; top: 0; width: 1000px; height: 300px; overflow: hidden;">
            <div>
                <a u="image" href="#">
                    <img src="/Images/landscape/02.jpg" alt="" /></a>
            </div>
            <div>
                <a u="image" href="#">

                    <img src="/Images/landscape/boy.png" alt="" /></a>
            </div>
        </div>
        <div u="navigator" class="jssorb05" style="position: absolute; bottom: 16px; right: 6px;">
            <div u="prototype" style="position: absolute; width: 16px; height: 16px;"></div>
        </div>
        <span u="arrowleft" class="jssora12l" style="width: 30px; height: 46px; top: 140px; left: 0;"></span>
        <span u="arrowright" class="jssora12r" style="width: 30px; height: 46px; top: 140px; right: 0"></span>
    </div>
</div>
