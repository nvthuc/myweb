﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.Data;
namespace MyWeb.ASC
{
    public partial class NewNews : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var ctx = new MyWebEntities())
                {
                    rptNewNews.DataSource = ctx.News.OrderBy(m => m.Newactive == 1).OrderBy(m => m.Newdate).Take(5).ToList();
                    rptNewNews.DataBind();
                }

            }
        }
    }
}