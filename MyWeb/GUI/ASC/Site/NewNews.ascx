﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewNews.ascx.cs" Inherits="MyWeb.ASC.NewNews" %>
<div class="row col-100">
    <h4 class="">Tin mới</h4>
</div>
<div class="clearfix"></div>
<div class="col-100">
    <asp:Repeater runat="server" ID="rptNewNews">
        <HeaderTemplate>
            <ul class="media-list">
        </HeaderTemplate>
        <ItemTemplate>
            <li class="media">
                <a href="/tu-van/<%#Eval("Keyname") %>-tv<%#Eval("Nid") %>.html">
                    <img src='<%#Eval("Newimage") %>' class="pull-left thumb img-thumb" alt="" width="40px" height="40px" />
                    <div class="media-body"><%#Eval("Title") %></div>
                </a>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>



</div>
