﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageMenu.ascx.cs" Inherits="MyWeb.GUI.ASC.Site.PageMenu" %>


<div class="row page-menu">
    <div class="col-xs-3 page-menu-item">
        <div class="thumbnail menu-item-thumb">
            <a href="/loai-san-pham">
                <img src="/Images/landscape/menu1.png" alt="" width="100%" height="135" /></a>
        </div>
        <div class="clearfix"></div>
        <h4><a href="/loai-san-pham">Loại sản phẩm</a></h4>
        <asp:Repeater ID="rptCate" runat="server">
            <HeaderTemplate>
                <ul class="list-unstyled">
            </HeaderTemplate>
            <ItemTemplate>
                <li><a href='/san-pham/<%#Eval("Keyname") %>_c<%#Eval("Cid") %>'><%#Eval("Catname") %></a></li>
            </ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
    </div>

    <div class="col-xs-3 page-menu-item">
        <div class="thumbnail menu-item-thumb">
            <a href="/nha-san-xuat">
                <img src="/Images/landscape/menu1.png" alt="" width="100%" height="135" /></a>
        </div>
        <div class="clearfix"></div>
        <h4><a href="/nha-san-xuat">Nhà sản xuất</a></h4>
        <asp:Repeater ID="rptBrands" runat="server">
            <HeaderTemplate>
                <ul class="list-unstyled">
            </HeaderTemplate>
            <ItemTemplate>
                <li><a href='/san-pham/<%#MyWeb.Business.Function.NameToTag(Eval("Tite").ToString())%>_b<%#Eval("Brandid") %>'><%#Eval("Tite") %></a></li>
            </ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
    </div>

    <div class="col-xs-3 page-menu-item">
        <div class="thumbnail menu-item-thumb">
            <a href="#">
                <img src="/Images/landscape/menu1.png" alt="" width="100%" height="135" /></a>
        </div>
        <div class="clearfix"></div>
        <h4><a href="/tu-van">Tư vấn</a></h4>
        <asp:Repeater ID="rptTuVan" runat="server">
            <HeaderTemplate>
                <ul class="list-unstyled">
            </HeaderTemplate>
            <ItemTemplate>
                <li><a href='/tu-van/<%#(Eval("Keyname").ToString())%>-tv<%#Eval("Nid") %>.html'><%#Eval("Title") %></a></li>
            </ItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
        </asp:Repeater>
    </div>

    <div class="col-xs-3 page-menu-item">
        <div class="thumbnail menu-item-thumb">
            <a href="#">
                <img src="/Images/landscape/menu1.png" alt="" width="100%" height="135" /></a>
        </div>
        <div class="clearfix"></div>
        <h4><a>Hỗ trợ trực tuyến</a></h4>
        <p class="support">
            <asp:Literal Text="" runat="server" ID="ltrSupport" />
        </p>
        <h4><a>Hotline</a></h4>
        <p class="support">
            <asp:Literal Text="" runat="server" ID="ltrHotline" />
        </p>
    </div>
</div>
