﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Navbar.ascx.cs" Inherits="MyWeb.GUI.ASC.Site.Navbar" %>
<div class="row">
    <div class="navbar navbar-mynav" role="navigation">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse mycollapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/">TRANG CHỦ</a></li>
                <li><a href="/gioi-thieu">GIỚI THIỆU</a></li>
                <li><a href="/san-pham">SẢN PHẨM</a></li>
                <li><a href="/tu-van">TƯ VẤN</a></li>
                <li><a href="/lien-he" class="last">LIÊN HỆ</a></li>
            </ul>
            <div class="navbar-form navbar-right mysearch " role="search">
                <div class="input-group ">
                    <input type="text" class="form-control" placeholder="Tìm kiếm">
                    <span class="input-group-btn ">
                        <button class="btn btn-default" type="button">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</div>
