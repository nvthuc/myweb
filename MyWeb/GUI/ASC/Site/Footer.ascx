﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="MyWeb.GUI.ASC.Site.Footer" %>
<div class="container-fluid footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 pull-left">
                <address>
                    <b>
                        <asp:Literal Text="" runat="server" ID="ltrCompany" /></b>
                    <p>
                        <asp:Literal Text="" runat="server" ID="ltrAddress" />
                        <br />
                        <asp:Literal Text="" runat="server" ID="ltrNote2" />
                    </p>
                </address>
            </div>
            <div class="col-xs-col-2 pull-right " id="copyright">
                Designed by &copy;VMS
            </div>
        </div>
    </div>
</div>
