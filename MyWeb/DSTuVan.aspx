﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/main.Master" AutoEventWireup="true" CodeBehind="DSTuVan.aspx.cs" Inherits="MyWeb.DSTuVan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-my-panel">
                <div class="panel-heading">
                    <h2 class="panel-title">
                        Tư vấn</h2>
                </div>
                <div class="panel-body">
                    <asp:Repeater ID="rptTv" runat="server">
                        <ItemTemplate>
                            <div class="panel panel-default pnTV">
                                <div class="panel-body">
                                    <h4><a href='/tu-van/<%#Eval("Keyname") %>_tv<%#Eval("Nid") %>.html'><%#Eval("Title") %></a></h4>
                                    <p><%#Eval("Newcontent") %></p>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
