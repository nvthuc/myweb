﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/main.Master" AutoEventWireup="true" CodeBehind="DatHang.aspx.cs" Inherits="MyWeb.DatHang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-my-panel">
                <div class="panel-heading">
                    <h2 class="panel-title">Đặt hàng</h2>
                </div>
                <asp:Panel runat="server" ID="pnForm" Visible="true">
                    <div class="panel-body">
                        <p>
                            Bạn vui lòng điền đầy đủ thông tin vào mẫu sau:
                        <br />
                            (Bỏ trống thông tin người nhận nếu bạn trực tiếp nhận hàng)
                        </p>
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Thông tin người mua</h4>
                                <div class="form">
                                    <div class="form-group">
                                        <label for="<%#txtBillName.ClientID %>">Họ và tên</label>
                                        <asp:TextBox runat="server" type="email" CssClass="form-control" ID="txtBillName" placeholder="họ và tên" />
                                    </div>

                                    <div class="form-group">
                                        <label for="<%#txtBillPhone.ClientID %>">Số điện thoại</label>
                                        <asp:TextBox runat="server" type="email" CssClass="form-control" ID="txtBillPhone" placeholder="số điện thoại" />
                                    </div>

                                    <div class="form-group">
                                        <label for="<%#txtBillAddress.ClientID %>">Địa chỉ</label>
                                        <asp:TextBox runat="server" type="email" CssClass="form-control" ID="txtBillAddress" placeholder="địa chỉ" TextMode="MultiLine" Rows="2" />
                                    </div>
                                </div>
                            </div>
                            <%--<div class="col-md-6">
                            <h4>Thông tin người nhận hàng</h4>
                            <div class="form">
                                <div class="form-group">
                                    <label for="<%#txtCusName.ClientID %>">Họ và tên</label>
                                    <asp:TextBox runat="server" type="email" CssClass="form-control" ID="txtCusName" placeholder="họ và tên" />
                                </div>

                                <div class="form-group">
                                    <label for="<%#txtCusPhone.ClientID %>">Số điện thoại</label>
                                    <asp:TextBox runat="server" type="email" CssClass="form-control" ID="txtCusPhone" placeholder="số điện thoại" />
                                </div>

                                <div class="form-group">
                                    <label for="<%#txtCusAddress.ClientID %>">Địa chỉ</label>
                                    <asp:TextBox runat="server" type="email" CssClass="form-control" ID="txtCusAddress" placeholder="địa chỉ" TextMode="MultiLine" Rows="2" />
                                </div>
                            </div>
                        </div>--%>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form">
                                    <div class="form-group">
                                        <label for="<%#txtBillRequest.ClientID %>">Yêu cầu</label>
                                        <asp:TextBox runat="server" type="email" CssClass="form-control" ID="txtBillRequest" placeholder="yêu cầu" TextMode="MultiLine" Rows="2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Danh sách sản phẩm</h4>
                                <asp:Repeater runat="server" ID="rptCart">
                                    <HeaderTemplate>
                                        <table class="table table-bordered table-hover">
                                            <tr>
                                                <th>Tên sản phẩm</th>
                                                <th>Số lượng</th>
                                                <th>Giá</th>
                                                <th>Thành tiền</th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <img src='<%#Eval("Image") %>' width="40px" height="40px" /><%#Eval("Name") %>
                                            </td>
                                            <td><%#Eval("Number") %></td>
                                            <td><%#double.Parse(Eval("Price").ToString()).ToString("C0") %></td>
                                            <td><%#double.Parse(Eval("Money").ToString()).ToString("C0") %></td>

                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr>
                                            <td colspan="6" class="text-right">Tổng tiền: <b>
                                                <asp:Literal Text="" runat="server" ID="ltrTotalMoney" /></b> </td>
                                        </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <asp:Button Text="Đặt mua" runat="server" CssClass="btn btn-primary" ID="btnDatMua" OnClick="btnDatMua_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnThank" Visible="false">
                    <div class="panel-body">
                        <div class="alert alert-success" role="alert">Cảm ơn bạn đã đặt hàng. Chúng tôi sẽ liên hệ với bạn để xác nhận giao hàng sớm nhât có thể.</div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>

