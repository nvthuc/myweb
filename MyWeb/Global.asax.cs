﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
namespace MyWeb
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            //Rigister route
            RegisterRoute(RouteTable.Routes);

        }


        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

        //Register route
        private void RegisterRoute(RouteCollection r)
        {

            r.MapPageRoute("Trang gioi thieu", "gioi-thieu", "~/about.aspx");
            r.MapPageRoute("Lien he", "lien-he", "~/contact.aspx");

            //Hien thi san pham theo loai sua
            r.MapPageRoute("san pham theo loai", "san-pham/{t}_c{cid}", "~/Default.aspx", false, new RouteValueDictionary() { { "Section", "ProByCate" } });
            

            //Hien thi san pham theo nha sna xuat
            r.MapPageRoute("nha san xuat", "san-pham/{t}_b{bid}", "~/Default.aspx", false, new RouteValueDictionary() { { "Section", "ProByBrand" } });

            //Chi tiet san pham
            r.MapPageRoute("chi tiet san pham", "san-pham/{t}_p{pid}.html", "~/ProductDetail.aspx");

            //Danh sach loai san pham
            r.MapPageRoute("danh sach loai pham", "loai-san-pham", "~/Default.aspx", false, new RouteValueDictionary() { { "Section", "Cates" } });
            r.MapPageRoute("danh sach loai pham theo loai san pham", "loai-san-pham/{t}_c{cid}", "~/Default.aspx", false, new RouteValueDictionary() { { "Section", "CatesByCate" } });

            r.MapPageRoute("danh sach NSX", "nha-san-xuat", "~/Default.aspx", false, new RouteValueDictionary() { { "Section", "Brands" } });

            r.MapPageRoute("danh sach sp", "san-pham", "~/Default.aspx", false, new RouteValueDictionary() { { "Section", "Pros" } });

            //Gio hang
            r.MapPageRoute("gio hang", "gio-hang", "~/giohang.aspx");
            r.MapPageRoute("Them san pham", "gio-hang/them/{pid}", "~/giohang.aspx");
            r.MapPageRoute("Dat hang", "dat-hang", "~/DatHang.aspx");

            r.MapPageRoute("danh sach tu van", "tu-van", "~/dstuvan.aspx");
            r.MapPageRoute("chi tiet tu van", "tu-van/{t}_tv{id}.html", "~/tuvan.aspx");

        }
    }
}
