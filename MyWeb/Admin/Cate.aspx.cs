﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MyWeb.Data;
using MyWeb.Business;
namespace MyWeb.Admin
{
    public partial class Cate : System.Web.UI.Page
    {

        #region global variable
        static bool flAddNew = false;
        static int CurentID = 0;
        static string ParentTreeCode = "";
        #endregion

        #region Page Event


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var pCode = Request.QueryString["pCode"];
                if (pCode != null && !string.IsNullOrEmpty(pCode.ToString()))
                {
                    ParentTreeCode = pCode.ToString();
                    LoadBreadcrumb();
                }
                else ParentTreeCode = "";
                LoadData();
            }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            flAddNew = true;
            CurentID = 0;
            pnTable.Visible = false;
            pnEdit.Visible = true;
            ResetControl();

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int cou = 0;
            foreach (GridViewRow row in grvTable.Rows)
            {
                using (var ctx = new MyWebEntities())
                {
                    CheckBox chkSele = row.Cells[0].FindControl("chkDel") as CheckBox;
                    if (chkSele.Checked)
                    {
                        Label lbti = row.Cells[0].FindControl("lblId") as Label;
                        int id = int.Parse(lbti.Text);
                        ctx.Categories.DeleteObject(ctx.Categories.Where(m => m.Cid == id).FirstOrDefault());
                        ctx.SaveChanges();
                        cou++;
                    }
                }
            }
            MyWeb.Business.Function.WebAlert("Deleted " + cou + " record(s) !");
            LoadData();
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            pnEdit.Visible = false;
            pnTable.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (var ctx = new MyWebEntities())
            {
                if (flAddNew)
                {

                    var obj = new Data.Category();
                    obj.Catname = Catname.Text;
                    obj.Icon = Icon.Text;
                    obj.Catcontent = Content.Text;
                    obj.Catactive = Catactive.Checked == true ? 1 : 0;
                    obj.Catord = int.Parse(Catord.Text);
                    obj.Treecode = SetCateTreeCode(int.Parse(Catord.Text));
                    obj.Keyname = SetCategoryKeyName(Catname.Text);

                    ctx.Categories.AddObject(obj);
                    ctx.SaveChanges();

                    pnEdit.Visible = false;
                    pnTable.Visible = true;

                    MyWeb.Business.Function.WebAlert(Response, "Saved !");
                    LoadData();
                }
                else
                {

                    var obj = ctx.Categories.Where(m => m.Cid == CurentID).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Catname = Catname.Text;
                        obj.Icon = Icon.Text;
                        obj.Catcontent = Content.Text;
                        obj.Catactive = Catactive.Checked == true ? 1 : 0;
                        obj.Keyname = SetCategoryKeyName(Catname.Text);
                        obj.Catord = int.Parse(Catord.Text);
                        ctx.SaveChanges();
                        MyWeb.Business.Function.WebAlert(Response, "Updated !");
                        pnEdit.Visible = false;
                        pnTable.Visible = true;

                        LoadData();
                    }
                }
            }
        }

        protected void grvTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("DeleteRecord"))
            {
                int id = int.Parse(e.CommandArgument.ToString());
                using (var ctx = new MyWebEntities())
                {
                    ctx.Categories.DeleteObject(ctx.Categories.Where(m => m.Cid == id).FirstOrDefault());
                    ctx.SaveChanges();
                    MyWeb.Business.Function.WebAlert(Response, "Deleted !");
                    LoadData();
                }
            }
            else if (e.CommandName.Equals("EditRecord"))
            {
                flAddNew = false;
                CurentID = int.Parse(e.CommandArgument.ToString());
                pnTable.Visible = false;
                pnEdit.Visible = true;
                BindData(CurentID);
            }
            else if (e.CommandName.Equals("ToggleActive"))
            {
                //Toggle class
                foreach (GridViewRow row in grvTable.Rows)
                {
                    Label lbti = row.Cells[0].FindControl("lblId") as Label;
                    int reid = int.Parse(lbti.Text);
                    if (reid == int.Parse(e.CommandArgument.ToString()))
                    {
                        LinkButton lbtact = row.Cells[1].FindControl("lbtActiveTogle") as LinkButton;
                        lbtact.Text = ToggleActive(reid);
                        //LoadData();
                        return;
                    }
                }
            }
            //else if (e.CommandName.Equals("ViewChild"))
            //{
            //    string code = e.CommandArgument.ToString();

            //    Response.Redirect("cate.aspx?pCode="+code);
            //}

        }

        protected void chkDelAll_CheckedChanged(object sender, EventArgs e)
        {
            var chkAll = sender as CheckBox;
            foreach (GridViewRow item in grvTable.Rows)
            {
                CheckBox chk = item.Cells[0].FindControl("chkDel") as CheckBox;
                chk.Checked = !chk.Checked;
            }
        }

        private void ResetControl()
        {
            Catname.Text = "";
            Icon.Text = "";
            Content.Text = "";
            Catord.Text = "";
        }

        private string ToggleActive(int reid)
        {
            using (var ctx = new MyWebEntities())
            {
                var re = ctx.Categories.Where(m => m.Cid == reid).FirstOrDefault();
                if (re != null)
                {
                    if (re.Catactive == 0)
                    {
                        re.Catactive = 1;
                        ctx.SaveChanges();
                        return "<i class='glyphicon glyphicon-ok-circle'></i>";
                    }
                    else
                    {
                        re.Catactive = 0;
                        ctx.SaveChanges();
                        return "<i class='glyphicon glyphicon-ban-circle'></i>";
                    }

                }
                return "";
            }

        }

        private void BindData(int id)
        {
            using (var ctx = new MyWebEntities())
            {
                var obj = ctx.Categories.Where(m => m.Cid == id).FirstOrDefault();
                if (obj != null)
                {
                    Content.Text = obj.Catcontent;
                    Catname.Text = obj.Catname;
                    Icon.Text = obj.Icon;
                    Catactive.Checked = obj.Catactive == 0 ? false : true;
                    string treecode = obj.Treecode;
                    treecode = treecode.Substring(treecode.Length - 5, 5);
                    Catord.Text = int.Parse(treecode).ToString();
                }
            }

        }

        private void LoadData()
        {
            using (var ctx = new MyWebEntities())
            {
                int deepth = ParentTreeCode.Length + 5;
                grvTable.DataSource = ctx.Categories.Where(m => m.Treecode.Length == deepth && m.Treecode.StartsWith(ParentTreeCode)).OrderBy(m => m.Treecode).ToList();
                grvTable.DataBind();
            }
        }

        protected string ActiveStatus(string st)
        {
            if (st.Equals("0"))
            {
                return "glyphicon glyphicon-ban-circle";
            }
            return "glyphicon glyphicon-ok-circle";
        }

        #endregion

        #region TreeCode
        private string SetCateTreeCode(int ord)
        {
            using (var ctx = new MyWebEntities())
            {
                //Current treecode 
                string ctree = GenTreeCode(ParentTreeCode, ord);
                //Lenght of current treecode
                int treeLenght = ParentTreeCode.Length + 5;
                //check treecode
                if (ctx.Categories.Where(m => m.Treecode == ctree).Count() > 0)
                {
                    //da co trong csdl
                    //cac tre cung level
                    var samedepth = ctx.Categories.Where(m => m.Treecode.Length == treeLenght).OrderByDescending(m => m.Treecode).ToList();
                    foreach (var item in samedepth)
                    {
                        string itTreeCode = item.Treecode;
                        int itOrd = TreeCodeToOrd(itTreeCode);
                        if (itOrd >= ord)
                        {
                            item.Treecode = GenTreeCode(ParentTreeCode, itOrd + 1);
                            string itNewTreeCode = item.Treecode;
                            foreach (var citem in ctx.Categories.Where(m => m.Treecode.StartsWith(itTreeCode)).OrderByDescending(m => m.Treecode))
                            {
                                int citLenght = citem.Treecode.Length;
                                string lastTree = citem.Treecode.Substring(treeLenght, citLenght - treeLenght);
                                citem.Treecode = itNewTreeCode + lastTree;
                            }
                        }
                    }
                    ctx.SaveChanges();
                    return GenTreeCode(ParentTreeCode, ord);
                }
                else
                {
                    //chua co trong csdl
                    //Lay max tree ord
                    var cmaxtree = ctx.Categories.Where(m => m.Treecode.Length == treeLenght).OrderByDescending(m => m.Treecode).FirstOrDefault();
                    if (cmaxtree == null)
                    {
                        return GenTreeCode(ParentTreeCode, 1);
                    }
                    string maxtree = cmaxtree.Treecode;
                    int maxOrd = TreeCodeToOrd(maxtree);
                    return GenTreeCode(ParentTreeCode, maxOrd + 1);
                }

            }
        }
        private string GenTreeCode(string Parent, int ord)
        {
            string strOrd = ord.ToString();
            while (strOrd.Length < 5)
            {
                strOrd = "0" + strOrd;
            }
            return Parent + strOrd;
        }
        private int TreeCodeToOrd(string trecode)
        {
            int lenght = trecode.Length;
            trecode = trecode.Substring(lenght - 5, 5);
            return int.Parse(trecode);
        }
        #endregion

        #region Private method

        private string SetCategoryKeyName(string name)
        {
            string key = MyWeb.Business.Function.NameToTag(name);
            int c = 1;
            do
            {
                if (CheckKeyName(key))
                {
                    return key;
                }
                key += c++;
            } while (true);
        }
        private bool CheckKeyName(string key)
        {
            using (var ctx = new MyWebEntities())
            {
                if (ctx.Categories.Where(m => m.Keyname == key).Count() > 0)
                {
                    return false;
                }
                return true;
            }

        }




        private void LoadBreadcrumb()
        {
            if (ParentTreeCode.Length > 0)
            {
                using (var ctx = new MyWebEntities())
                {
                    ltrBreadcrumb.Text = "";
                    string cCode = ParentTreeCode;
                    do
                    {
                        int cLenght = cCode.Length;

                        var cate = ctx.Categories.Where(m => m.Treecode == cCode).FirstOrDefault();
                        if (cate != null)
                        {
                            ltrBreadcrumb.Text = " <li><a href='?pCode=" + cCode + "'>" + cate.Catname + "</a></li>" + ltrBreadcrumb.Text;
                        }
                        cCode = cCode.Substring(0, cLenght - 5);
                    } while (cCode != "");
                }
            }
        }





        protected string CutParentCode(string co)
        {
            int lenght = co.Length;
            return co.Substring(0, lenght - 5);
        }
        #endregion
    }
}