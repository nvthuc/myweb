﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.Data;
namespace MyWeb.Admin
{
    public partial class Product : System.Web.UI.Page
    {
        static bool flAddNew = false;
        static int CurrentID = 0;
        static bool flAddProp = true;
        static int CurrentPropID = 0;
        static int CurrentCate = 0;
        static int CurrentBrand = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Page.Title = "Quản lý sản phẩm";
                //Tải dữ liệu vào bảng
                LoadData();
                //Tải danh sách NSX
                LoadBrands();
                //Tải danh sách Loại SP
                LoadCate("", "", 5);
            }
        }

        //Show form to add product
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            flAddNew = true;
            CurrentID = 0;
            pnTable.Visible = false;
            pnEdit.Visible = true;
            ResetControl();

        }

        //Delete multi product
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int cou = 0;
            foreach (GridViewRow row in grvTable.Rows)
            {
                using (var ctx = new MyWebEntities())
                {
                    CheckBox chkSele = row.Cells[0].FindControl("chkDel") as CheckBox;
                    if (chkSele.Checked)
                    {
                        Label lbti = row.Cells[0].FindControl("lblId") as Label;
                        int id = int.Parse(lbti.Text);

                        //Delete image
                        foreach (var item in ctx.Pro_Images.Where(m => m.Proid == id))
                        {
                            ctx.Pro_Images.DeleteObject(item);
                        }

                        //Delete properties
                        foreach (var item in ctx.Pro_Properties.Where(m => m.Proid == id))
                        {
                            ctx.Pro_Properties.DeleteObject(item);
                        }

                        //Delete product
                        ctx.Products.DeleteObject(ctx.Products.Where(m => m.Proid == id).FirstOrDefault());
                        ctx.SaveChanges();
                        cou++;
                    }
                }
            }
            MyWeb.Business.Function.WebAlert("Deleted " + cou + " record(s) !");
            LoadData();
        }


        #region Product table

        protected void grvTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("DeleteRecord"))
            {
                int id = int.Parse(e.CommandArgument.ToString());
                using (var ctx = new MyWebEntities())
                {
                    ctx.Products.DeleteObject(ctx.Products.Where(m => m.Proid == id).FirstOrDefault());
                    ctx.SaveChanges();
                    MyWeb.Business.Function.WebAlert(Response, "Deleted !");
                    LoadData();
                }
            }
            else if (e.CommandName.Equals("EditRecord"))
            {
                flAddNew = false;
                CurrentID = int.Parse(e.CommandArgument.ToString());
                pnTable.Visible = false;
                pnEdit.Visible = true;
                BindData(CurrentID);
            }
            else if (e.CommandName.Equals("ToggleActive"))
            {
                //Toggle class
                foreach (GridViewRow row in grvTable.Rows)
                {
                    Label lbti = row.Cells[0].FindControl("lblId") as Label;
                    int reid = int.Parse(lbti.Text);
                    if (reid == int.Parse(e.CommandArgument.ToString()))
                    {
                        LinkButton lbtact = row.Cells[1].FindControl("lbtActiveTogle") as LinkButton;
                        lbtact.Text = ToggleActive(reid);
                        //LoadData();
                        return;
                    }
                }
            }
            else if (e.CommandName.Equals("ViewImages"))
            {
                //View image
                CurrentID = int.Parse(e.CommandArgument.ToString());
                pnTable.Visible = false;
                pnEdit.Visible = false;
                pnImages.Visible = true;
                LoadImgList();
            }
            else if (e.CommandName.Equals("Properties"))
            {
                //View image
                CurrentID = int.Parse(e.CommandArgument.ToString());
                pnTable.Visible = false;
                pnEdit.Visible = false;
                pnImages.Visible = false;
                pnProperties.Visible = true;
                CurrentPropID = 0;
                flAddProp = true;
                LoadProps();
            }
        }



        private void LoadImgList()
        {
            using (var ctx = new MyWebEntities())
            {
                rptImg.DataSource = ctx.Pro_Images.Where(m => m.Proid == CurrentID).ToList();
                rptImg.DataBind();
            }

        }

        protected void chkDelAll_CheckedChanged(object sender, EventArgs e)
        {
            var chkAll = sender as CheckBox;
            foreach (GridViewRow item in grvTable.Rows)
            {
                CheckBox chk = item.Cells[0].FindControl("chkDel") as CheckBox;
                chk.Checked = !chk.Checked;
            }
        }

        private void ResetControl()
        {
            Name.Text = "";
            Quantity.Text = "";
            Price.Text = "";
            ProUnit.Text = "";

            Detail.Text = "";
            Tag.Text = "";
            ProImage.Text = "";
            Active.Checked = true;

        }

        private string ToggleActive(int reid)
        {
            using (var ctx = new MyWebEntities())
            {
                var re = ctx.Products.Where(m => m.Proid == reid).FirstOrDefault();
                if (re != null)
                {
                    if (re.Proactive == 0)
                    {
                        re.Proactive = 1;
                        ctx.SaveChanges();
                        return "<i class='glyphicon glyphicon-ok-circle'></i>";
                    }
                    else
                    {
                        re.Proactive = 0;
                        ctx.SaveChanges();
                        return "<i class='glyphicon glyphicon-ban-circle'></i>";
                    }

                }
                return "<i class='glyphicon glyphicon-ban-circle'></i>";
            }

        }

        private void BindData(int id)
        {
            using (var ctx = new MyWebEntities())
            {
                var obj = ctx.Products.Where(m => m.Proid == id).FirstOrDefault();
                if (obj != null)
                {
                    Name.Text = obj.Proname;
                    try
                    {
                        Brands.SelectedValue = obj.Brandid.ToString();
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        Cate.SelectedValue = obj.Catid.ToString();
                    }
                    catch (Exception)
                    {


                    }
                    Quantity.Text = obj.Number.ToString();
                    Price.Text = obj.Price.ToString();
                    ProUnit.Text = obj.Prounit;

                    Detail.Text = obj.Detail;
                    Tag.Text = obj.Tags;
                    Active.Checked = obj.Proactive == 1 ? true : false;
                    ProImage.Text = obj.Proimage;

                }
            }

        }

        private void LoadData()
        {
            using (var ctx = new MyWebEntities())
            {
                var ds = ctx.Products.ToList();
                if (CurrentBrand != 0)
                {
                    ds = ds.Where(m => m.Brandid == CurrentBrand).ToList();
                }
                if (CurrentCate != 0)
                {
                    ds = ds.Where(m => m.Catid == CurrentCate).ToList();
                }

                grvTable.DataSource = ds;
                grvTable.DataBind();
            }
        }

        protected string ActiveStatus(string st)
        {
            if (st.Equals("0"))
            {
                return "glyphicon glyphicon-ban-circle";
            }
            return "glyphicon glyphicon-ok-circle";
        }

        #endregion


        #region Form Product

        //Load data for dropdown list Categories
        private void LoadCate(string pre, string parent, int lenght)
        {
            using (var ctx = new MyWebEntities())
            {
                foreach (var item in ctx.Categories.Where(m => m.Treecode.Length == lenght && m.Treecode.StartsWith(parent)))
                {
                    Cate.Items.Add(new ListItem(pre + item.Catname, item.Cid.ToString(), true));
                    ddlCateFilter.Items.Add(new ListItem(pre + item.Catname, item.Cid.ToString(), true));
                    LoadCate(pre + "--", item.Treecode, lenght + 5);
                }
            }

        }

        //Load data for dropdown list Brands
        private void LoadBrands()
        {
            using (var ctx = new MyWebEntities())
            {
                foreach (var brand in ctx.Brands)
                {
                    ddlBrandFilter.Items.Add(new ListItem() { Enabled = true, Text = brand.Tite, Value = brand.Brandid.ToString() });
                    Brands.Items.Add(new ListItem() { Enabled = true, Text = brand.Tite, Value = brand.Brandid.ToString() });

                }
                Brands.DataBind();
                ddlBrandFilter.DataBind();

            }

        }

        //Back to table pro
        protected void btnBack_Click(object sender, EventArgs e)
        {
            pnEdit.Visible = false;
            pnTable.Visible = true;
        }

        //Save to database
        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (var ctx = new MyWebEntities())
            {
                if (flAddNew)
                {
                    var obj = new Data.Product();

                    obj.Proname = Name.Text;
                    obj.Brandid = int.Parse(Brands.SelectedValue);
                    obj.Catid = int.Parse(Cate.SelectedValue);
                    obj.Number = int.Parse(Quantity.Text);
                    obj.Price = double.Parse(Price.Text);
                    obj.Prounit = ProUnit.Text;

                    obj.Detail = Detail.Text;
                    obj.Tags = Tag.Text;
                    obj.Proactive = Active.Checked ? 1 : 0;
                    obj.Proimage = ProImage.Text;

                    ctx.Products.AddObject(obj);

                    ctx.SaveChanges();


                    MyWeb.Business.Function.WebAlert(Response, "Saved !");
                    LoadData();

                    var newPro = ctx.Products.OrderByDescending(m => m.Proid).FirstOrDefault();
                    if (newPro ==null)
                    {
                        pnEdit.Visible = false;
                        pnTable.Visible = true;
                        return;

                    }
                   
                    CurrentID = newPro.Proid;
                    pnEdit.Visible = false;
                    pnProperties.Visible = true;
                    LoadProps();
                }
                else
                {
                    var obj = ctx.Products.Where(m => m.Proid == CurrentID).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Proname = Name.Text;
                        obj.Brandid = int.Parse(Brands.SelectedValue);
                        obj.Catid = int.Parse(Cate.SelectedValue);
                        obj.Number = int.Parse(Quantity.Text);
                        obj.Price = double.Parse(Price.Text);
                        obj.Prounit = ProUnit.Text;

                        obj.Detail = Detail.Text;
                        obj.Tags = Tag.Text;
                        obj.Proactive = Active.Checked ? 1 : 0;
                        obj.Proimage = ProImage.Text;

                        ctx.SaveChanges();
                        MyWeb.Business.Function.WebAlert(Response, "Updated !");
                        pnEdit.Visible = false;
                        pnTable.Visible = true;
                        LoadData();
                    }
                }
            }
        }

        #endregion

        #region Image manager
        protected void AddImage_Click(object sender, EventArgs e)
        {
            if (Image.Text != null)
            {
                using (var ctx = new MyWebEntities())
                {
                    ctx.Pro_Images.AddObject(new Data.Pro_Images() { Proid = CurrentID, Imageoriginal = Image.Text, Imagethumb = "/Uploads/_thumbs/" + Image.Text.Substring(9) });
                    ctx.SaveChanges();
                    LoadImgList();
                }

            }
        }

        protected void Unnamed_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName.ToString().Equals("Del"))
            {
                using (var ctx = new MyWebEntities())
                {
                    int id = int.Parse(e.CommandArgument.ToString());
                    ctx.Pro_Images.DeleteObject(ctx.Pro_Images.FirstOrDefault(m => m.Id == id));
                    ctx.SaveChanges();
                    LoadImgList();
                }

            }
        }
        protected void b_Click(object sender, EventArgs e)
        {
            pnImages.Visible = false;
            pnTable.Visible = true;
        }

        #endregion

        #region Properties manager
        private void LoadProps()
        {
            using (var ctx = new MyWebEntities())
            {
                grvProp.DataSource = ctx.Pro_Properties.Where(m => m.Proid == CurrentID).ToList();
                grvProp.DataBind();
            }

        }
        protected void btnSaveProp_Click(object sender, EventArgs e)
        {
            using (var ctx = new MyWebEntities())
            {
                if (flAddProp)
                {
                    var prop = new Data.Pro_Properties();
                    prop.Lable = label.Text;
                    prop.Content = value.Text;
                    prop.Proid = CurrentID;
                    prop.Active = chkPropActive.Checked ? 1 : 0;
                    ctx.Pro_Properties.AddObject(prop);
                    ctx.SaveChanges();
                    MyWeb.Business.Function.WebAlert("Saved !");
                    label.Text = "";
                    value.Text = "";
                    LoadProps();
                }
                else
                {
                    var prop = ctx.Pro_Properties.FirstOrDefault(m => m.ID == CurrentPropID);

                    prop.Lable = label.Text;
                    prop.Content = value.Text;
                    prop.Active = chkPropActive.Checked ? 1 : 0;
                    ctx.SaveChanges();
                    MyWeb.Business.Function.WebAlert("Saved !");
                    label.Text = "";
                    value.Text = "";
                    CurrentPropID = 0;
                    flAddProp = true;
                    LoadProps();

                }
            }

        }
        protected void btnBackToTable_Click(object sender, EventArgs e)
        {
            pnProperties.Visible = false;
            pnTable.Visible = true;
            flAddProp = true;
            CurrentPropID = 0;

        }
        protected void grvProp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRecord")
            {
                int id = int.Parse(e.CommandArgument.ToString());
                using (var ctx = new MyWebEntities())
                {
                    ctx.Pro_Properties.DeleteObject(ctx.Pro_Properties.FirstOrDefault(m => m.ID == id));
                    ctx.SaveChanges();
                    MyWeb.Business.Function.WebAlert("Deleted !");
                    LoadProps();
                }
            }
            else if (e.CommandName == "EditRecord")
            {
                flAddProp = false;
                CurrentPropID = int.Parse(e.CommandArgument.ToString());
                using (var ctx = new MyWebEntities())
                {
                    var prop = ctx.Pro_Properties.FirstOrDefault(m => m.ID == CurrentPropID);
                    if (prop != null)
                    {
                        label.Text = prop.Lable;
                        value.Text = prop.Content;
                        chkPropActive.Checked = prop.Active == 1 ? true : false;
                    }
                }
            }
        }
        protected void chkDelAll_CheckedChanged1(object sender, EventArgs e)
        {
            var chkAll = sender as CheckBox;
            foreach (GridViewRow item in grvProp.Rows)
            {
                CheckBox chk = item.Cells[0].FindControl("chkDel") as CheckBox;
                chk.Checked = !chk.Checked;
            }
        }

        #endregion

        protected void btnReset_Click(object sender, EventArgs e)
        {
            CurrentPropID = 0;
            flAddProp = true;
            value.Text = "";
            label.Text = "";
        }

        protected void ddlCateFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentCate = int.Parse(ddlCateFilter.SelectedValue);
            LoadData();
        }

        protected void ddlBrandFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentBrand = int.Parse(ddlBrandFilter.SelectedValue);
            LoadData();
        }
    }
}