﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/Admin.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="MyWeb.Admin.Contact" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeader" runat="server">
    Liên hệ
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <asp:Panel runat="server" ID="pnTable">
            <div class="col-md-12">
                <asp:GridView runat="server" CssClass="table table-bordered table-hover" ID="grvTable" AutoGenerateColumns="false" OnRowCommand="grvTable_RowCommand">
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle Width="30px" />
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkDelAll" runat="server" OnCheckedChanged="chkDelAll_CheckedChanged" AutoPostBack="true" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label Text='<%#Eval("Id") %>' runat="server" Visible="false" ID="lblId" />
                                <asp:CheckBox ID="chkDel" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle Width="40px" />
                            <HeaderTemplate></HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton Text="" CssClass="btn btn-danger btn-xs" runat="server" OnClientClick="return confirm('Are you sure to delete ?');" CommandName="DeleteRecord" CommandArgument='<%#Eval("Id") %>'>
                                        <i class='glyphicon glyphicon-remove'></i>
                                </asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle Width="200px" />
                            <HeaderTemplate>Tên</HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("Fullname") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle Width="200px" />
                            <HeaderTemplate>Số điện thoại</HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("Phone") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Nội dung</HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("idea1") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="col-md-12">
                <div class="pull-right">
                    <ul class="pagination">
                        <asp:Literal Text="" runat="server" ID="ltrPagination" />
                    </ul>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
