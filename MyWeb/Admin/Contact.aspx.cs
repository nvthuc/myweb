﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using MyWeb.Data;
namespace MyWeb.Admin
{
    public partial class Contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int pageIndex = 1;
                if (Request.QueryString["page"] != null)
                {
                    pageIndex = int.Parse(Request.QueryString["page"].ToString());
                }
                int pageCount = 1;
                int pageSize = 20;
                LoadContact(pageIndex,pageSize, ref pageCount);
                ltrPagination.Text = LoadPagingation(pageIndex, pageCount);
            }
        }
        private string LoadPagingation(int PageIndex, int PageCount)
        {
            if (PageCount > 0)
            {
                //First
                string result = "<li><a href='?page=1'>&laquo;</a></li>";

                //... if > 1
                if (PageIndex > 3)
                {
                    result += "<li><a>...</a></li>";
                }
                if (PageIndex == 3)
                {
                    result += "<li><a href='?page=1'>1</a></li>";
                }
                //1 not view
                if (PageIndex > 1)
                {
                    result += "<li><a href='?page=" + (PageIndex - 1) + "'>" + (PageIndex - 1) + "</a></li>";
                }
                //if (PageIndex == 1)
                //{
                //    ltrPagination.Text += "<li><a href='?page=1'>1</a></li>";
                //}
                //Current
                result += "<li class='active'><a href='#'>" + (PageIndex) + "</a></li>";
                //
                if (PageIndex + 1 <= PageCount)
                {
                    result += "<li><a href='?page=" + (PageIndex + 1) + "'>" + (PageIndex + 1) + "</a></li>";
                }
                if (PageIndex == 1 && PageCount >= 3)
                {
                    result += "<li><a href='?page=3'>3</a></li>";
                }
                if (PageCount > 3 && PageIndex < PageCount - 3)
                {
                    result += "<li><a>...</a></li>";
                }
                result += "<li><a href='?page=" + PageCount + "'>&raquo;</a></li>";
                return result;
            }
            else
                return "";
        }
        private void LoadContact(int pageIndex,int pageSize, ref int pageCount)
        {
            using (var ctx = new MyWebEntities())
            {
                grvTable.DataSource = ctx.Ideas.OrderBy(m => m.Status).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                grvTable.DataBind();
                int tt = ctx.Ideas.Count();
                if (tt > 0)
                {
                    pageCount = (tt % pageSize == 0) ? (tt / pageSize) : ((tt / pageSize) + 1);
                }
            }

        }

        protected void grvTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
           
        }

        protected void chkDelAll_CheckedChanged(object sender, EventArgs e)
        {
            var chkAll = sender as CheckBox;
            foreach (GridViewRow item in grvTable.Rows)
            {
                CheckBox chk = item.Cells[0].FindControl("chkDel") as CheckBox;
                chk.Checked = !chk.Checked;
            }
        }
    }
}