﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MyWeb.Data;
namespace MyWeb.Admin
{
    public partial class Brand : System.Web.UI.Page
    {
        static bool flAddNew = false;
        static int CurrentId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            using (var ctx = new MyWebEntities())
            {
                grvTable.DataSource = ctx.Brands.ToList();
                grvTable.DataBind();
            }

        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            flAddNew = true;
            CurrentId = 0;
            pnTable.Visible = false;
            pnEdit.Visible = true;
            ResetControl();

        }

        private void ResetControl()
        {
            txtName.Text = "";
            txtLogo.Text = "";
            txtDes.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int cou = 0;
            foreach (GridViewRow row in grvTable.Rows)
            {
                using (var ctx = new MyWebEntities())
                {
                    CheckBox chkSele = row.Cells[0].FindControl("chkDel") as CheckBox;
                    if (chkSele.Checked)
                    {
                        Label lbti = row.Cells[0].FindControl("lblId") as Label;
                        int id = int.Parse(lbti.Text);
                        ctx.Brands.DeleteObject(ctx.Brands.Where(m => m.Brandid == id).FirstOrDefault());
                        ctx.SaveChanges();
                        cou++;
                    }
                }
            }
            MyWeb.Business.Function.WebAlert("Deleted " + cou + " record(s) !");
            LoadData();
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            pnEdit.Visible = false;
            pnTable.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (flAddNew)
            {
                var obj = new Data.Brand();
                obj.Tite = txtName.Text;
                obj.Logo = txtLogo.Text;
                obj.Note = txtDes.Text;
               
                using (var ctx = new MyWebEntities())
                {
                    ctx.Brands.AddObject(obj);
                    ctx.SaveChanges();
                }
                pnEdit.Visible = false;
                pnTable.Visible = true;
                LoadData();
                MyWeb.Business.Function.WebAlert(Response, "Saved !");
            }
            else
            {
                using (var ctx = new MyWebEntities())
                {
                    var obj = ctx.Brands.Where(m => m.Brandid == CurrentId).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Tite = txtName.Text;
                        obj.Logo = txtLogo.Text;
                        obj.Note = txtDes.Text;
                        ctx.SaveChanges();
                        MyWeb.Business.Function.WebAlert(Response, "Updated !");
                        pnEdit.Visible = false;
                        pnTable.Visible = true;
                        LoadData();
                    }
                }

            }
        }

        protected void grvTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("DeleteRecord"))
            {
                try
                {
                    int id = int.Parse(e.CommandArgument.ToString());
                    using (var ctx = new MyWebEntities())
                    {
                        ctx.Brands.DeleteObject(ctx.Brands.Where(m => m.Brandid == id).FirstOrDefault());
                        ctx.SaveChanges();
                        MyWeb.Business.Function.WebAlert(Response, "Deleted !");
                    }
                    LoadData();
                }
                catch (Exception ex)
                {
                    MyWeb.Business.Function.WebAlert(Response, ex.GetType().Name + " : " + ex.Message);
                }
            }
            else if (e.CommandName.Equals("EditRecord"))
            {
                flAddNew = false;
                CurrentId = int.Parse(e.CommandArgument.ToString());
                pnTable.Visible = false;
                pnEdit.Visible = true;
                BindData(CurrentId);
            }
           
        }

        private string ToggleActive(int reid)
        {
            using (var ctx = new MyWebEntities())
            {
                var re = ctx.News.Where(m => m.Nid == reid).FirstOrDefault();
                if (re != null)
                {
                    if (re.Newactive == 0)
                    {
                        re.Newactive = 1;
                        ctx.SaveChanges();
                        return "<i class='glyphicon glyphicon-ok-circle'></i>";
                    }
                    else
                    {
                        re.Newactive = 0;
                        ctx.SaveChanges();
                        return "<i class='glyphicon glyphicon-ban-circle'></i>";
                    }

                }

                return "";
            }

        }

        private void BindData(int id)
        {
            using (var ctx = new MyWebEntities())
            {
                var obj = ctx.Brands.Where(m => m.Brandid == id).FirstOrDefault();
                if (obj != null)
                {
                    txtName.Text = obj.Tite;
                    txtDes.Text = obj.Note;
                    txtLogo.Text = obj.Logo;
                   
                }
            }

        }

        protected void chkDelAll_CheckedChanged(object sender, EventArgs e)
        {
            var chkAll = sender as CheckBox;
            foreach (GridViewRow item in grvTable.Rows)
            {
                CheckBox chk = item.Cells[0].FindControl("chkDel") as CheckBox;
                chk.Checked = !chk.Checked;
            }
        }
      
    }
}