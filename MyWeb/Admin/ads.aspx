﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/Admin.Master" AutoEventWireup="true" CodeBehind="ads.aspx.cs" Inherits="MyWeb.Admin.ads" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeader" runat="server">
    Ảnh quảng cáo
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <asp:DropDownList runat="server" ID="ddlImgType" AutoPostBack="true" OnSelectedIndexChanged="ddlImgType_SelectedIndexChanged">
                    <asp:ListItem Text="Logo" Value="0" />
                    <asp:ListItem Text="Banner" Value="1" />
                    <asp:ListItem Text="Menu 1" Value="2" />
                    <asp:ListItem Text="Menu 2" Value="3" />
                    <asp:ListItem Text="Menu 3" Value="4" />
                    <asp:ListItem Text="Menu 4" Value="5" />
                    <asp:ListItem Text="Quảng cáo bên phải" Value="6" />
                </asp:DropDownList>
            </div>
            <asp:Panel runat="server" ID="pnTable">
                <div class="col-md-12">
                    <asp:Button Text="Thêm" runat="server" ID="btnAddNew" CssClass="btn btn-primary" OnClick="btnAddNew_Click" />
                    <asp:Button Text="Xóa" runat="server" ID="btnDelete" CssClass="btn btn-danger" OnClientClick="return confirm('Are you sure to delete selected record ?');" OnClick="btnDelete_Click" />
                    <br />
                    <br />
                </div>
               
                <div class="col-md-12">
                    <asp:GridView
                        runat="server"
                        CssClass="table table-bordered table-hover"
                        ID="grvTable"
                        AutoGenerateColumns="false"
                        OnRowCommand="grvTable_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px" />
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkDelAll" runat="server" OnCheckedChanged="chkDelAll_CheckedChanged" AutoPostBack="true" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label Text='<%#Eval("Proid") %>' runat="server" Visible="false" ID="lblId" />
                                    <asp:CheckBox ID="chkDel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle Width="100px" />
                                <HeaderTemplate>Chức năng</HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton Text="" CssClass="btn btn-danger btn-xs" runat="server" OnClientClick="return confirm('Are you sure to delete ?');" CommandName="DeleteRecord" CommandArgument='<%#Eval("Proid") %>'>
                                        <i class='glyphicon glyphicon-remove'></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton Text="" CssClass="btn btn-primary btn-xs" runat="server" CommandName="EditRecord" CommandArgument='<%#Eval("Proid") %>'>
                                        <i class='glyphicon glyphicon-pencil'></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton ToolTip="" CssClass="btn btn-primary btn-xs" runat="server" CommandName="ToggleActive" CommandArgument='<%#Eval("Proid") %>' ID="lbtActiveTogle">
                                        <i class='<%#ActiveStatus(Eval("Proactive").ToString()) %>'></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton ToolTip="Hình ảnh" CssClass="btn btn-primary btn-xs" runat="server" CommandName="ViewImages" CommandArgument='<%#Eval("Proid") %>'>
                                        <span class="glyphicon glyphicon-picture"></span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ToolTip="Thuộc tính" CssClass="btn btn-primary btn-xs" runat="server" CommandName="Properties" CommandArgument='<%#Eval("Proid") %>'>
                                        <span class="glyphicon glyphicon-th-list"></span>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>Tiêu đề</HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("Proname") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>Số lượng</HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("Number") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnEdit" Visible="false">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Tên</label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Nhà SX</label>
                        <div class="col-md-4">
                            <asp:DropDownList runat="server" ID="Brands" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Loại</label>
                        <div class="col-md-4">
                            <asp:DropDownList runat="server" ID="Cate" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Số lượng</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="Quantity" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Giá</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="Price" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Đơn vị</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="ProUnit" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Chi tiết</label>
                        <div class="col-md-10">
                            <CKEditor:CKEditorControl ID="Detail" runat="server"></CKEditor:CKEditorControl>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Tag</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="Tag" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Hình ảnh</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="ProImage" CssClass="form-control" />
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-default" onclick="BrowseServerFile('<%=ProImage.ClientID %>');">Duyệt ảnh</button>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Hiển thị</label>
                        <div class="col-md-10">
                            <asp:CheckBox Text="" runat="server" ID="Active" Checked="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-lg-offset-2">
                            <asp:Button runat="server" CssClass="btn btn-primary" Text="Lưu" ID="btnSave" OnClick="btnSave_Click" />
                            <asp:Button runat="server" CssClass="btn btn-default" Text="Trở lại" ID="btnBack" OnClick="btnBack_Click" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
    <script>
        $(function () {

        });
        function TimFile(id) {
            var f = new CKFinder();
            f.selectActionFunction = function (fileUrl) {
                document.getElementById(id).value = fileUrl;
                document.getElementById("imgpreview").src = "/Uploads/_thumbs/" + fileUrl.substr(9);
            };
            f.popup();
        }
    </script>
</asp:Content>
