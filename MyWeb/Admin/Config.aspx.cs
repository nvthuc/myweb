﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.Data;
namespace MyWeb.Admin
{
    public partial class Config : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var ctx = new MyWebEntities())
                {
                    var config = ctx.Configs.FirstOrDefault();
                    if (config != null)
                    {
                        txtSlogan.Text = config.Slogan;
                        txtSloganDes.Text = config.Note1;
                        txtSiteName.Text = config.Title;
                        txtSup1.Text = config.Email;
                        txtSup2.Text = config.Conemail;
                        txtHot1.Text = config.Tel;
                        txtHot2.Text = config.Fax;
                        txtAdd.Text = config.Address;
                        txtNote2.Text = config.Note2;
                      
                        txtGioiThieu.Text = config.Copyright;
                    }
                }

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (var ctx = new MyWebEntities())
            {
                var config = ctx.Configs.FirstOrDefault();

                if (config == null) config = new Data.Config();

                config.Slogan = txtSlogan.Text;
                config.Note1 = txtSloganDes.Text;
                config.Title = txtSiteName.Text;
                config.Email = txtSup1.Text;
                config.Conemail = txtSup2.Text;
                config.Tel = txtHot1.Text;
                config.Fax = txtHot2.Text;
                config.Address = txtAdd.Text;
                config.Note2 = txtNote2.Text;
              
                config.Copyright = txtGioiThieu.Text;
                ctx.SaveChanges();
                MyWeb.Business.Function.WebAlert(Response, "Saved !");
            }
        }
    }
}