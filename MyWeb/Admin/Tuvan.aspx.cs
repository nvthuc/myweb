﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.Data;
namespace MyWeb.Admin
{
    public partial class Tuvan : System.Web.UI.Page
    {
        static bool flAddNew = false;
        static int CurrentID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            using (var ctx = new MyWebEntities())
            {
                grvTable.DataSource = ctx.News.ToList();
                grvTable.DataBind();
            }

        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            flAddNew = true;
            CurrentID = 0;
            pnTable.Visible = false;
            pnEdit.Visible = true;
            ResetControl();

        }

        private void ResetControl()
        {
            txtTitle.Text = "";
            txtThumb.Text = "";
            txtDes.Text = "";
            txtContent.Text = "";
            txtSource.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int cou = 0;
            foreach (GridViewRow row in grvTable.Rows)
            {
                using (var ctx = new MyWebEntities())
                {
                    CheckBox chkSele = row.Cells[0].FindControl("chkDel") as CheckBox;
                    if (chkSele.Checked)
                    {
                        Label lbti = row.Cells[0].FindControl("lblId") as Label;
                        int id = int.Parse(lbti.Text);
                        ctx.News.DeleteObject(ctx.News.Where(m => m.Nid == id).FirstOrDefault());
                        ctx.SaveChanges();
                        cou++;
                    }
                }
            }
            MyWeb.Business.Function.WebAlert("Deleted " + cou + " record(s) !");
            LoadData();
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            pnEdit.Visible = false;
            pnTable.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (flAddNew)
            {
                var obj = new Data.News();
                obj.Title = txtTitle.Text;
                obj.Newimage = txtThumb.Text;
                obj.Newcontent = txtDes.Text;
                obj.Detail = txtContent.Text;
                obj.Newdate = DateTime.Today;
                obj.Writer = txtSource.Text;
                obj.Newactive = chkView.Checked == true ? 1 : 0;
                obj.Keyname = MyWeb.Business.Function.NameToTag(txtTitle.Text);
                using (var ctx = new MyWebEntities())
                {
                    ctx.News.AddObject(obj);
                    ctx.SaveChanges();
                }
                pnEdit.Visible = false;
                pnTable.Visible = true;
                LoadData();
                MyWeb.Business.Function.WebAlert(Response, "Saved !");
            }
            else
            {
                using (var ctx = new MyWebEntities())
                {
                    var obj = ctx.News.Where(m => m.Nid == CurrentID).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Title = txtTitle.Text;
                        obj.Newimage = txtThumb.Text;
                        obj.Newcontent = txtDes.Text;
                        obj.Detail = txtContent.Text;
                        obj.Newdate = DateTime.Today;
                        obj.Writer = txtSource.Text;
                        obj.Newactive = chkView.Checked == true ? 1 : 0;
                        obj.Keyname = MyWeb.Business.Function.NameToTag(txtTitle.Text);
                        ctx.SaveChanges();
                        MyWeb.Business.Function.WebAlert(Response, "Updated !");
                        pnEdit.Visible = false;
                        pnTable.Visible = true;
                        LoadData();
                    }
                }

            }
        }

        protected void grvTable_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("DeleteRecord"))
            {
                int id = int.Parse(e.CommandArgument.ToString());
                using (var ctx = new MyWebEntities())
                {
                    ctx.News.DeleteObject(ctx.News.Where(m => m.Nid == id).FirstOrDefault());
                    ctx.SaveChanges();
                    MyWeb.Business.Function.WebAlert(Response, "Deleted !");
                    LoadData();
                }

            }
            else if (e.CommandName.Equals("EditRecord"))
            {
                flAddNew = false;
                CurrentID = int.Parse(e.CommandArgument.ToString());
                pnTable.Visible = false;
                pnEdit.Visible = true;
                BindData(CurrentID);
            }
            else if (e.CommandName.Equals("ToggleActive"))
            {
                //Toggle class
                foreach (GridViewRow row in grvTable.Rows)
                { 
                    Label lbti = row.Cells[0].FindControl("lblId") as Label;
                    int reid = int.Parse(lbti.Text);
                    if (reid == int.Parse(e.CommandArgument.ToString()))
                    {
                        LinkButton lbtact = row.Cells[1].FindControl("lbtActiveTogle") as LinkButton;
                        lbtact.Text = ToggleActive(reid);
                      // LoadData();
                        return;
                    }

                }
            }
        }

        private string ToggleActive(int reid)
        {
            using (var ctx = new MyWebEntities())
            {
                var re = ctx.News.Where(m => m.Nid == reid).FirstOrDefault();
                if (re != null)
                {
                    if (re.Newactive == 0)
                    {
                        re.Newactive = 1;
                        ctx.SaveChanges();
                        return "<i class='glyphicon glyphicon-ok-circle'></i>";
                    }
                    else
                    {
                        re.Newactive = 0;
                        ctx.SaveChanges();
                        return "<i class='glyphicon glyphicon-ban-circle'></i>";
                    }
                   
                }
               
                return "";
            }

        }

        private void BindData(int id)
        {
            using (var ctx = new MyWebEntities())
            {
                var obj = ctx.News.Where(m => m.Nid == id).FirstOrDefault();
                if (obj != null)
                {
                    txtContent.Text = obj.Detail;
                    txtDes.Text = obj.Newcontent;
                    txtSource.Text = obj.Writer;
                    txtThumb.Text = obj.Newimage;
                    txtTitle.Text = obj.Title;
                }
            }

        }

        protected void chkDelAll_CheckedChanged(object sender, EventArgs e)
        {
            var chkAll = sender as CheckBox;
            foreach (GridViewRow item in grvTable.Rows)
            {
                CheckBox chk = item.Cells[0].FindControl("chkDel") as CheckBox;
                chk.Checked = !chk.Checked;
            }
        }
        protected string ActiveStatus(string st)
        {
            if (st.Equals("0"))
            {
                return "glyphicon glyphicon-ban-circle";
            }
            return "glyphicon glyphicon-ok-circle";
        }
    }
}