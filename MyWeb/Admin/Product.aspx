﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/Admin.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="MyWeb.Admin.Product" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeader" runat="server">
    Sản phẩm
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:Panel runat="server" ID="pnTable">
                <div class="col-md-12">
                    <asp:Button Text="Thêm" runat="server" ID="btnAddNew" CssClass="btn btn-primary" OnClick="btnAddNew_Click" />
                    <asp:Button Text="Xóa" runat="server" ID="btnDelete" CssClass="btn btn-danger" OnClientClick="return confirm('Are you sure to delete selected record ?');" OnClick="btnDelete_Click" />
                    <br />
                    <br />
                </div>
                <div class="col-md-12">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Lọc sản phẩm: </label>
                            <div class="col-md-2">
                                <asp:DropDownList runat="server" ID="ddlCateFilter" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCateFilter_SelectedIndexChanged">
                                    <asp:ListItem Selected="True" Enabled="true" Text="Loại sản phẩm" Value="0" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList runat="server" ID="ddlBrandFilter" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlBrandFilter_SelectedIndexChanged">
                                    <asp:ListItem Selected="True" Enabled="true" Text="Hãng sản xuất" Value="0" />
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    
                   
                </div>
                <div class="col-md-12">
                    <asp:GridView
                        runat="server"
                        CssClass="table table-bordered table-hover"
                        ID="grvTable"
                        AutoGenerateColumns="false"
                        OnRowCommand="grvTable_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px" />
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkDelAll" runat="server" OnCheckedChanged="chkDelAll_CheckedChanged" AutoPostBack="true" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label Text='<%#Eval("Proid") %>' runat="server" Visible="false" ID="lblId" />
                                    <asp:CheckBox ID="chkDel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle Width="160px" />
                                <HeaderTemplate>Chức năng</HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton Text="" CssClass="btn btn-danger btn-xs" runat="server" OnClientClick="return confirm('Are you sure to delete ?');" CommandName="DeleteRecord" CommandArgument='<%#Eval("Proid") %>'>
                                        <i class='glyphicon glyphicon-remove'></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton Text="" CssClass="btn btn-primary btn-xs" runat="server" CommandName="EditRecord" CommandArgument='<%#Eval("Proid") %>'>
                                        <i class='glyphicon glyphicon-pencil'></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton ToolTip="" CssClass="btn btn-primary btn-xs" runat="server" CommandName="ToggleActive" CommandArgument='<%#Eval("Proid") %>' ID="lbtActiveTogle">
                                        <i class='<%#ActiveStatus(Eval("Proactive").ToString()) %>'></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton ToolTip="Hình ảnh" CssClass="btn btn-primary btn-xs" runat="server" CommandName="ViewImages" CommandArgument='<%#Eval("Proid") %>'>
                                        <span class="glyphicon glyphicon-picture"></span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ToolTip="Thuộc tính" CssClass="btn btn-primary btn-xs" runat="server" CommandName="Properties" CommandArgument='<%#Eval("Proid") %>'>
                                        <span class="glyphicon glyphicon-th-list"></span>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>Tiêu đề</HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("Proname") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>Số lượng</HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("Number") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>


                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnEdit" Visible="false">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Tên</label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Nhà SX</label>
                        <div class="col-md-4">
                            <asp:DropDownList runat="server" ID="Brands" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Loại</label>
                        <div class="col-md-4">
                            <asp:DropDownList runat="server" ID="Cate" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Số lượng</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="Quantity" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Giá</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="Price" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Đơn vị</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="ProUnit" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Chi tiết</label>
                        <div class="col-md-10">
                            <CKEditor:CKEditorControl ID="Detail" runat="server"></CKEditor:CKEditorControl>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Tag</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="Tag" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Hình ảnh</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="ProImage" CssClass="form-control" />
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-default" onclick="BrowseServerFile('<%=ProImage.ClientID %>');">Duyệt ảnh</button>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Hiển thị</label>
                        <div class="col-md-10">
                            <asp:CheckBox Text="" runat="server" ID="Active" Checked="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-lg-offset-2">
                            <asp:Button runat="server" CssClass="btn btn-primary" Text="Lưu" ID="btnSave" OnClick="btnSave_Click" />
                            <asp:Button runat="server" CssClass="btn btn-default" Text="Trở lại" ID="btnBack" OnClick="btnBack_Click" />
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" Visible="false" ID="pnImages">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Thêm ảnh</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="Image" CssClass="form-control" />
                            <div class="thumbnail img-thumbnail">
                                <img src="" alt="" id="imgpreview" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-default" onclick="TimFile('<%=Image.ClientID %>');">Duyệt ảnh</button>
                            <asp:Button Text="Lưu" CssClass="btn btn-primary" ID="AddImage" OnClick="AddImage_Click" runat="server" />
                            <asp:Button Text="Trở lại" CssClass="btn btn-primary" ID="b" OnClick="b_Click" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <asp:Repeater runat="server" ID="rptImg">
                    <HeaderTemplate>
                        <h3 class="page-header">Danh sách ảnh</h3>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="col-md-2">
                            <div class="thumbnail img-thumbnail">
                                <img src='<%#Eval("Imagethumb") %>' alt="" />
                            </div>
                            <asp:LinkButton Text="Xóa" CommandArgument='<%#Eval("Id") %>' CommandName="Del" OnCommand="Unnamed_Command" CssClass="btn btn-danger pull-right" runat="server" />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnProperties" Visible="false">
                <div class="row col-md-12">
                    <h3>Thuộc tính sản phẩm</h3>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail" class="col-md-3 control-label">Thuộc tính</label>
                                <div class="col-md-8">
                                    <asp:TextBox runat="server" ID="label" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-md-3 control-label">Giá trị</label>
                                <div class="col-md-8">
                                    <asp:TextBox runat="server" ID="value" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-md-3 control-label">Hiển thị</label>
                                <div class="col-md-8">
                                    <asp:CheckBox Text="" ID="chkPropActive" Checked="true" runat="server" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-3">
                                    <asp:Button runat="server" CssClass="btn btn-primary" Text="Lưu" ID="btnSaveProp" OnClick="btnSaveProp_Click" />
                                    <asp:Button runat="server" CssClass="btn btn-primary" Text="Xóa" ID="btnReset" OnClick="btnReset_Click" />
                                    <asp:Button runat="server" CssClass="btn btn-default" Text="Trở lại" ID="btnBackToTable" OnClick="btnBackToTable_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <asp:GridView
                            runat="server"
                            CssClass="table table-bordered table-hover"
                            ID="grvProp"
                            AutoGenerateColumns="false"
                            OnRowCommand="grvProp_RowCommand">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle Width="30px" />
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkDelAll" runat="server" OnCheckedChanged="chkDelAll_CheckedChanged1" AutoPostBack="true" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label Text='<%#Eval("ID") %>' runat="server" Visible="false" ID="lblId" />
                                        <asp:CheckBox ID="chkDel" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle Width="100px" />
                                    <HeaderTemplate>Chức năng</HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton Text="" CssClass="btn btn-danger btn-xs" runat="server" OnClientClick="return confirm('Are you sure to delete ?');" CommandName="DeleteRecord" CommandArgument='<%#Eval("ID") %>'>
                                        <i class='glyphicon glyphicon-remove'></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton Text="" CssClass="btn btn-primary btn-xs" runat="server" CommandName="EditRecord" CommandArgument='<%#Eval("ID") %>'>
                                        <i class='glyphicon glyphicon-pencil'></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ToolTip="" CssClass="btn btn-primary btn-xs" runat="server" CommandName="ToggleActive" CommandArgument='<%#Eval("ID") %>' ID="lbtActiveTogleProp">
                                        <i class='<%#ActiveStatus(Eval("Active").ToString()) %>'></i>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>Thuộc tính</HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("Lable") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>Giá trị</HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("Content") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>


            </asp:Panel>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
    <script>
        $(function () {

        });
        function TimFile(id) {
            var f = new CKFinder();
            f.selectActionFunction = function (fileUrl) {
                document.getElementById(id).value = fileUrl;
                document.getElementById("imgpreview").src = "/Uploads/_thumbs/" + fileUrl.substr(9);
            };
            f.popup();
        }
    </script>
</asp:Content>
