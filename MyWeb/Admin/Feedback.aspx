﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/Admin.Master" AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="MyWeb.Admin.Feedback" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeader" runat="server">
    Phản hồi từ khách hàng
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Tên Website</label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtSiteName" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Slogan</label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" ID="txtSlogan" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Mô tả Slogan</label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" ID="txtSloganDes" TextMode="MultiLine" Rows="3" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Hỗ trợ 1</label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtSup1" TextMode="SingleLine" Rows="3" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Hỗ trợ 2</label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtSup2" TextMode="SingleLine" Rows="3" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Hotline 1</label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtHot1" TextMode="SingleLine" Rows="3" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Hotline 2</label>
                    <div class="col-md-4">
                        <asp:TextBox runat="server" ID="txtHot2" TextMode="SingleLine" Rows="3" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Địa chỉ</label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" ID="txtAdd" TextMode="MultiLine" Rows="3" CssClass="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail" class="col-md-2 control-label">Ghi chú</label>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" ID="txtNote2" CssClass="form-control" />
                    </div>
                </div>
               

                <div class="form-group">
                    <div class="col-md-10 col-lg-offset-2">
                        <asp:Button runat="server" CssClass="btn btn-primary" Text="Lưu cài đặt" ID="btnSave" />
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
