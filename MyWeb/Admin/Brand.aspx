﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/Admin.Master" AutoEventWireup="true" CodeBehind="Brand.aspx.cs" Inherits="MyWeb.Admin.Brand" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageHeader" runat="server">
    Nhà Sản Xuất
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:Panel runat="server" ID="pnTable">
                <div class="col-md-12">
                    <asp:Button Text="Add New" runat="server" ID="btnAddNew" CssClass="btn btn-primary" OnClick="btnAddNew_Click" />
                    <asp:Button Text="Delete" runat="server" ID="btnDelete" CssClass="btn btn-danger" OnClientClick="return confirm('Are you sure to delete selected record ?');" OnClick="btnDelete_Click" />
                    <br />
                    <br />
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <asp:GridView runat="server" CssClass="table table-bordered table-hover" ID="grvTable" AutoGenerateColumns="false" OnRowCommand="grvTable_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px" />
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkDelAll" CssClass="" runat="server" OnCheckedChanged="chkDelAll_CheckedChanged" AutoPostBack="true" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label Text='<%#Eval("Brandid") %>' runat="server" Visible="false" ID="lblId" />
                                    <asp:CheckBox ID="chkDel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle Width="100px" />
                                <HeaderTemplate>Chức năng</HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton Text="" CssClass="btn btn-danger btn-xs" runat="server" OnClientClick="return confirm('Are you sure to delete ?');" CommandName="DeleteRecord" CommandArgument='<%#Eval("Brandid") %>'>
                                        <i class='glyphicon glyphicon-remove'></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton Text="" CssClass="btn btn-primary btn-xs" runat="server" CommandName="EditRecord" CommandArgument='<%#Eval("Brandid") %>'>
                                        <i class='glyphicon glyphicon-pencil'></i>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                
                                <HeaderTemplate>Logo</HeaderTemplate>
                                <ItemTemplate>
                                    <img src='<%#Eval("Logo") %>' alt='<%#Eval("Tite") %>' width="50px" height="50px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>Tên</HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("Tite") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnEdit" Visible="false">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Tên</label>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="txtName" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Logo</label>
                        <div class="col-md-4">
                            <asp:TextBox runat="server" ID="txtLogo" CssClass="form-control" ReadOnly="true" />
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-default" onclick="javascript:return BrowseServerFile('<%=txtLogo.ClientID %>');">Browse server </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-md-2 control-label">Mô tả</label>
                        <div class="col-md-10">

                            <CKEditor:CKEditorControl ID="txtDes" runat="server"></CKEditor:CKEditorControl>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10 col-lg-offset-2">
                            <asp:Button runat="server" CssClass="btn btn-primary" Text="Lưu" ID="btnSave" OnClick="btnSave_Click" />
                            <asp:Button runat="server" CssClass="btn btn-default" Text="Trở lại" ID="btnBack" OnClick="btnBack_Click" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
