﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MyWeb.Data;
namespace MyWeb
{
    public partial class ProductDetail : System.Web.UI.Page
    {
        static int Proid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var proid = RouteData.Values["pid"] as string;
                if (proid == null)
                {
                    Response.End();
                    return;
                }
                int.TryParse(proid, out Proid);
                LoadData();
            }
        }

        private void LoadData()
        {
            //Load category
            using (var ctx = new MyWebEntities())
            {
                //Get product
                var pro = ctx.Products.FirstOrDefault(m => m.Proid == Proid);
                if (pro != null)
                {
                    //Page Title 
                    Page.Title = pro.Proname;

                    //category
                    int Cid = (int)pro.Catid;
                    var cate = ctx.Categories.FirstOrDefault(m => m.Cid == Cid);
                    if (cate != null)
                    {
                        ltrCateName.Text = cate.Catname;
                    }

                    //Proname
                    ltrProName.Text = pro.Proname;


                    //Pro image

                    ltrImgTarget.Text = "<img id='multizoom1' style='max-width:307px; max-height:300px;' alt='zoomable' src='"+pro.Proimage+"' />";
                    ltrImgPro.Text = "<a href='"+pro.Proimage+"'><img id='multizoom1' alt='zoomable' width='50px' height='50px' src='" + "/Uploads/_thumbs/"+pro.Proimage.Substring(9) + "' /></a>";

                    rptProImg.DataSource = ctx.Pro_Images.Where(m => m.Proid == Proid).ToList();
                    rptProImg.DataBind();

                    //Pro props
                    rptProProps.DataSource = ctx.Pro_Properties.Where(m => m.Proid == Proid && m.Active == 1).ToList();
                    rptProProps.DataBind();


                    //Nut Dat mua

                    ltrDatMua.Text = "<a href='/gio-hang/them/"+Proid+"' class='btn btn-danger btn-lg pull-right'>Đặt mua</a>";

                    //Pro detail
                    ltrProDetail.Text = pro.Detail;

                    //Same  type product
                    int cateid =(int) pro.Catid;
                    rptProduct.DataSource = ctx.Products.Where(p => p.Proactive == 1 && p.Catid == cateid).Take(12).ToList();
                    rptProduct.DataBind();
                }
                else Response.End();
            }

        }

    }
}