﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/main.Master" AutoEventWireup="true" CodeBehind="GioHang.aspx.cs" Inherits="MyWeb.GioHang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-my-panel">
                <div class="panel-heading">
                    <h2 class="panel-title">Giỏ hàng</h2>
                </div>
                <div class="panel-body">
                    <div class="row col-md-12">
                        <asp:Repeater runat="server" ID="rptCart" OnItemCommand="rptCart_ItemCommand">
                            <HeaderTemplate>
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <th>Tên sản phẩm</th>
                                        <th>Số lượng</th>
                                        <th>Giá</th>
                                        <th>Thành tiền</th>
                                        <th></th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                   
                                    <td>
                                        <img src='<%#Eval("Image") %>' width="40px" height="40px" /><%#Eval("Name") %>
                                    </td>
                                    <td>
                                        <asp:Label Text='<%#Eval("ID") %>'  ID="lblId" Visible="false" runat="server" />
                                        <asp:TextBox runat="server" ID="txtNumber" Text='<%#Eval("Number") %>' Width="50px" />
                                        <asp:LinkButton Text='<i class="glyphicon glyphicon-refresh"></i>' CommandName="UpdateNumber" CommandArgument='<%#Eval("ID") %>' runat="server" CssClass="btn btn-primary btn-xs" />

                                    </td>
                                    <td><%#double.Parse(Eval("Price").ToString()).ToString("C0") %></td>
                                    <td><%#double.Parse(Eval("Money").ToString()).ToString("C0") %></td>
                                    <td>
                                        <asp:LinkButton Text='<i class="glyphicon glyphicon-remove"></i>' OnClientClick="return confirm('Bạn có chắc chắn muốn xóa sản phẩm ?');" CommandName="DeleteProduct" CommandArgument='<%#Eval("ID") %>' runat="server" CssClass="btn btn-danger btn-xs" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                <tr>
                                    <td colspan="6" class="text-right">
                                       Tổng tiền: <b>
                                           <asp:Literal Text="" runat="server" ID="ltrTotalMoney" /></b> </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="row col-md-12">
                        <div class="pull-right">
                            <a href="/thanh-toan" class="btn btn-primary">Thanh toán</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
