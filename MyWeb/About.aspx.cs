﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.Data;
namespace MyWeb
{
    public partial class About : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "Giới thiệu";
            using (var ctx = new MyWebEntities())
            {
                var config = ctx.Configs.FirstOrDefault();
                if (config != null)
                {
                    ltrAbout.Text = config.Copyright;
                }
            }

        }
    }
}