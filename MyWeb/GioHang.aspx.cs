﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.Data;
using System.Data;
namespace MyWeb
{
    public partial class GioHang : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (RouteData.Values["pid"] != null)
                {
                    int Proid = int.Parse(RouteData.Values["pid"].ToString());
                    using (var ctx = new MyWebEntities())
                    {
                        if (Session["Cart"] == null)
                        {
                            Session["Cart"] = MyWeb.Business.ShopCart.CreateCart();
                        }
                        var cart = Session["Cart"] as System.Data.DataTable;
                        var pro = ctx.Products.FirstOrDefault(m => m.Proid == Proid);
                        MyWeb.Business.ShopCart.AddToCart(ref cart, Proid.ToString(), pro.Proname, pro.Proimage, (double)pro.Price);
                        Session["Cart"] = cart;
                        Response.Write("<script>history.go(-1);</script>");
                        Response.End();
                        return;
                    }
                }

                LoadCart();
            }
        }


        protected void rptCart_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToString())
            {
                case "UpdateNumber":
                    {
                        for (int i = 0; i < rptCart.Items.Count; i++)
                        {
                            var lblID = rptCart.Items[i].FindControl("lblId") as Label;
                            if (lblID.Text == e.CommandArgument.ToString())
                            {
                                var txtNumber = rptCart.Items[i].FindControl("txtNumber") as TextBox;
                                int newNumber = int.Parse(txtNumber.Text);
                                var ssCart = Session["Cart"] as DataTable;
                                MyWeb.Business.ShopCart.UpdateProduct(ref ssCart, int.Parse(e.CommandArgument.ToString()), newNumber);
                                Session["Cart"] = ssCart;
                                LoadCart();
                                break;
                            }
                        }
                        break;
                    }
                case "DeleteProduct":
                    {
                        var ssCart = Session["Cart"] as DataTable;
                        MyWeb.Business.ShopCart.DeleteProduct(ref ssCart, int.Parse(e.CommandArgument.ToString()));
                        Session["Cart"] = ssCart;
                        LoadCart();
                        break;
                    }
                default:
                    break;
            }
        }

        private void LoadCart()
        {
            var ssCart = Session["Cart"] as DataTable;
            rptCart.DataSource = ssCart;
            rptCart.DataBind();

            if (ssCart != null)
            {
                var ltrTotalMoney = rptCart.Controls[rptCart.Controls.Count - 1].FindControl("ltrTotalMoney") as Literal;
                ltrTotalMoney.Text = MyWeb.Business.ShopCart.TotalMoney(ssCart).ToString("C0");
            }
        }


    }
}