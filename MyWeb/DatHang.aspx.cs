﻿using MyWeb.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb
{
    public partial class DatHang : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var ds = Session["Cart"] as DataTable;
            if (ds != null)
            {
                rptCart.DataSource = ds;
                rptCart.DataBind();

                var ltrTotalMoney = rptCart.Controls[rptCart.Controls.Count - 1].FindControl("ltrTotalMoney") as Literal;
                ltrTotalMoney.Text = MyWeb.Business.ShopCart.TotalMoney(ds).ToString("C0");

            }
        }

        protected void btnDatMua_Click(object sender, EventArgs e)
        {
            using (var ctx = new MyWebEntities())
            {
                var Cus = new Data.CUSTOMER();
                Cus.Cusname = txtBillName.Text;
                Cus.Mobile = txtBillPhone.Text;
                Cus.Address = txtBillPhone.Text;
                ctx.CUSTOMERS.AddObject(Cus);
                ctx.SaveChanges();

                var cus = ctx.CUSTOMERS.OrderByDescending(m => m.Cusid).First();
                int cusID = cus.Cusid;

                var bill = new Data.Bill_customers();
                bill.Cusid = cusID;
                bill.Idate = DateTime.Now;
                bill.Xdate = new DateTime().AddDays(7);
                bill.Request = txtBillRequest.Text;
                var ltrTotalMoney = rptCart.Controls[rptCart.Controls.Count - 1].FindControl("ltrTotalMoney") as Literal;
                bill.Totalmoney = ltrTotalMoney.Text;
                bill.State = 0;
                ctx.Bill_customers.AddObject(bill);
                ctx.SaveChanges();

                var Bill = ctx.Bill_customers.OrderByDescending(m => m.Billid).First();
                int BillId = Bill.Billid;
                foreach (DataRow row in (Session["Cart"] as DataTable).Rows)
                {
                    var BD = new Billdetail();
                    BD.Bilid = BillId;
                    int ProId = int.Parse(row["ID"].ToString());
                    int ProNumber = int.Parse(row["Number"].ToString());
                    double ProPrice = double.Parse(row["Price"].ToString());
                    double ProMoney = double.Parse(row["Money"].ToString());
                    BD.Bilmoney = (decimal)ProMoney;
                    BD.Bilprice = (decimal)ProPrice;
                    BD.Quantity = ProNumber;
                    ctx.Billdetails.AddObject(BD);
                }
                ctx.SaveChanges();
                pnForm.Visible = false;
                pnThank.Visible = true;
            }

        }
    }
}