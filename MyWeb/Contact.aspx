﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/main.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="MyWeb.Contact" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-my-panel">
                <div class="panel-heading">
                    <h2 class="panel-title">Liên hệ</h2>
                </div>
                <div class="panel-body">
                    <asp:Panel runat="server" ID="pnForm" Visible="true">
                        <p>Hãy để lại thông tin của bạn, chúng tôi sẽ liên lạc với bạn.</p>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail" class="col-md-2 control-label">Tên </label>
                                <div class="col-md-4">
                                    <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-md-2 control-label">Số điện thoại </label>
                                <div class="col-md-4">
                                    <asp:TextBox runat="server" ID="Phone" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-md-2 control-label">Ý kiến </label>
                                <div class="col-md-10">
                                    <CKEditor:CKEditorControl ID="Idea" Toolbar="Basic" runat="server"></CKEditor:CKEditorControl>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-2">
                                    <asp:Button Text="Gửi" ID="btnSave" CssClass="btn btn-primary " OnClick="btnSave_Click" runat="server" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnAlert" Visible="false">
                        <div class="alert alert-success" role="alert">Cảm ơn bạn đã để lại thông tin. Chúng tôi sẽ liên hệ với bạn sớm nhât có thể.</div>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
