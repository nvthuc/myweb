﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Master/main.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="MyWeb.About" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-my-panel">
                <div class="panel-heading">
                    <h2 class="panel-title">Giới thiệu</h2>
                </div>
                <div class="panel-body">
                    <asp:Literal Text="" runat="server" ID="ltrAbout" />
                </div>
            </div>
        </div>
    </div>
   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
</asp:Content>
